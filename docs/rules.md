# Overview

The design guidelines for cards is that there should be no targeting required. Cards should interact with the board as a whole.

There are six card slots in total. On the top row are the heros of the story: a defensive hero, an offensive hero, and a support hero.
On the bottom row are the setting of the story. The environment, the weather, and an impediment.

# The board

Each player has a copy of their own board. 

# Player turn  

Every player's turn follows as such:

1. A player may first choose to discard a card of their choosing 
: If they discard a card, they may play 2 or draw 2 or pass
: Else they may play 1 or draw 1 or pass

2. When their turn is completed (explicitly registered), combat will initiate.

Heroes and impediments have attack/defense values. These persist and can be modified through combat. Damage isn't traded like in traditional TCGs.

a) The impediment 'attacks' the defense slot. If there is no defense slot active, it will attack the first hero from left to right.
b) The defense slot 'attacks' the impediment. If there is no impediment, the defense slot 'idles'.
c) The attack slot 'attacks' the impediment. If there is no impediment, the attack slot 'idles'
d) The support slot 'attacks' the impediment. If there is no impediment, the support slot 'idles'
e) If there is no defense slot, it will attack the first hero from left to right. Otherwise it 'idles'.
f) If the environment has an effect, it will trigger
f) If the weather has an effect, it will trigger 

# Card deployment

Hero cards have a role associated with them. When a hero card is played, it will be played into the respective slot on the board. If there is already
a hero in the specified slot it will be placed into the next slot available to the left. If there are no slots on the left, then it cannot be played.

Impediment and environment cards can only be played if there is no impediment. Otherwise the impediment must be removed before the journey can continue.

There are no weather type cards, the weather is always a side-effect.

Spells can be played at any time. They may have conditions on their effects, but can always be played (even if it results in doing nothing).

All cards are either 'positive' or 'negative'. If they are 'positive', they deploy to the board of the player who played it. If they are 'negative'
they deploy to the board of the opponent.

# Card mechanics

When slot A is deployed
: slot A "On deploy:" effect triggers
: any slots with "On deploys:" effect triggers against slot A

When slot A 'idles'
: slot A "On idle:" effect triggers

When slot A 'attacks' slot B
: slot A "On attack:" effect triggers, then
: if slot B is alive, its "On defend:" effect triggers

When slot A 'kills' slot B
: slot B "On death:" effect triggers, then
: slot A "On kill:" effect triggers

When an impediment dies
: All heros from left to right trigger their "On victory:" effect

When all heros die
: The impediment triggers its "On victory:" effect

When an environment changes
: All heros from left to right trigger their "On travel:" effect

When the weather changes
: All heroes from left to right trigger their "On weather:" effect

# Victory conditions

1. If the opponent concedes
2. If the game ends, and you have the most points. The game ends when one player has finished all their goals, or after 30 turns.

# Goal selection

Goals are secrets that are selected alongside the deck, which generate points when they are completed. Each player must select 1 goal from
each category: Easy, Normal, Hard.