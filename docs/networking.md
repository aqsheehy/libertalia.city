
## Networking...

Each turn players can make fixed decisions as determined by Turns. 
Side-effects of each decision may also result in a revelation.

A revelation is some kind of information that was secret to one or both players, and has been made available to one or both players. (e.g. Draw a card,
 play a card from the inactive hand).

All decisions and revelations are stored.
No matter which of the three views watching the game, if the same decisions are made, the same decision and revelation 
stack should be replayed.

At the end of the game, each player uploads 3 fields:
1. Hash of all decisions
2. Hash of all revelations
3. The player who won

0|decide|play|0
1|decide|play|3
2|decide|finish|
...
4|reveal|active|hand|0
5|reveal|inactive|hand|1
6|reveal|

If there is an inconsistency between the hashes, the server runs a game engine locally and decides who won.
