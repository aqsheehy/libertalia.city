import chai, { expect } from 'chai';
import { Card, CardDecorator } from '../src/engine/card.js';
import GameEngine from '../src/engine/game.js';

describe('Location', () => {
    it('should not freak out if it has not been initialized and is cleared', () => {
        var game = new GameEngine();
        var card = new Card(game);
        card.location.clear();
    });
});