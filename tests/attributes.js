import chai, { expect } from 'chai';
import AttributeStore from '../src/engine/attributes.js'

describe('AttributeStore', () => {
    
    it('should reinitialize when attributes are set', () => {
        var attributes = new AttributeStore();
        attributes.modifications = [{  }];
        attributes.set({ name: 'Sup!' });
        expect(attributes.modifications.length).to.equal(0);
    });
    
    it('should support explicit updates', () => {
        var attributes = new AttributeStore();
        attributes.set({ damage: 10 });
        attributes.patch({ op: 'modify', path: 'damage', value: 3 });
        expect(attributes.get().damage).to.equal(3);
    });
    
    it('should support explicit add updates', () => {
        var attributes = new AttributeStore();
        attributes.set({ damage: 10 });
        attributes.patch({ op: 'add', path: 'damage', value: 3 });
        expect(attributes.get().damage).to.equal(13);
    });
    
    it('should support explicit subtract updates', () => {
        var attributes = new AttributeStore();
        attributes.set({ damage: 10 });
        attributes.patch({ op: 'subtract', path: 'damage', value: 3 });
        expect(attributes.get().damage).to.equal(7);
    });
    
    it('should support explicit deletes', () => {
        var attributes = new AttributeStore();
        attributes.set({ damage: 10 });
        attributes.patch({ op: 'delete', path: 'damage'});
        expect(attributes.get().damage).to.equal(undefined);
    });
    
});