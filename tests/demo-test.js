import chai, { expect } from 'chai';
import { DefensiveHero } from '../src/engine/card.js'
import GameEngine from '../src/engine/game.js';

describe('Defensive Hero Cards', () => {
  it('Defensive Heros should be Combative', () => {
      var game = new GameEngine();
      var hero = new DefensiveHero(game);
      expect(hero.types).to.deep.equal([ 'Defensive', 'Hero', 'Combative', 'Card' ]);
  });
});