import chai, { expect } from 'chai';
import { Card, SupportiveHero, OffensiveHero, DefensiveHero } from '../src/engine/card.js'
import { CardCollection, HeroesCollection, Hand } from '../src/engine/cards.js'
import DEF001BOR from '../src/engine/cards/DEF001BOR.js';
import ATT001MOB from '../src/engine/cards/ATT001MOB.js';
import SP001ESC from '../src/engine/cards/SP001ESC.js';
import GameEngine from '../src/engine/game.js';

describe('CardCollection', () => {
  
  it('should have a length', () => {
    var game = new GameEngine();
    var cards = new CardCollection();
    cards.push(new Card(game));
    cards.push(new Card(game));
    cards.push(new Card(game));
    cards.push(new Card(game));
    expect(cards.length).to.equal(4);
  });
  
  it('should be concattenable', () => {
    var game = new GameEngine();
    var cards = new CardCollection();
    cards.push(new Card(game));
    cards.push(new Card(game));
    var concattable = cards.concat(new Card(game));
    expect(concattable.length).to.equal(3);
  });

  it('should enforce their max size', () => {
    var game = new GameEngine();
    var cards = new CardCollection(2);
    cards.push(new Card(game));
    cards.push(new Card(game));
    expect(() => { cards.push(new Card(game)); }).to.throw(Error);
  });

  it('should allow for querying against a specific type', () => {
    var game = new GameEngine();
    var cards = new CardCollection();
    cards.push(new DEF001BOR(game));
    cards.push(new SP001ESC(game));
    var orks = cards.ofType('Ork');
    expect(orks.length).to.equal(1);
  });

  it('should allow for querying against a specific type and maintain order', () => {
    var game = new GameEngine();
    var cards = new CardCollection();
    cards.push(new DEF001BOR(game));
    cards.push(new ATT001MOB(game));
    cards.push(new SP001ESC(game));
    var orks = cards.ofType('Ork');
    expect(orks.length).to.equal(2);
    expect(orks[0].id).to.equal('DEF001BOR');
    expect(orks[1].id).to.equal('ATT001MOB');
  });

  it('should allow for all cards to be modified', () => {
    var game = new GameEngine();
    var cards = new CardCollection();
    cards.push(new SP001ESC(game));
    cards.push(new SP001ESC(game));

    var orks = cards.ofType('Ork');
    expect(orks.length).to.equal(0);

    cards.modifyAll((card) => {
      return new DEF001BOR(game);
    });

    var orks = cards.ofType('Ork');
    expect(orks.length).to.equal(2);
  });

  it('should allow for cards to be deleted', () => {
    var game = new GameEngine();
    var cards = new CardCollection();
    cards.push(new SP001ESC(game));
    var toDelete = new SP001ESC(game);
    cards.push(toDelete);
    cards.push(new SP001ESC(game));

    expect(cards.length).to.equal(3);

    cards.remove(toDelete);
    
    expect(cards.indexOf(toDelete)).to.equal(-1);
    expect(cards.length).to.equal(2);
  });
  
  it('should be able to remove all cards that have "died"', () => {
    var cards = new CardCollection();
    var game = new GameEngine();
    var card = new DefensiveHero(game);
    cards.push(card);
    cards.clearDead();
    expect(cards.length).to.equal(1);
    card.death();
    expect(cards.clearDead()[0]).to.equal(card);
    expect(cards.length).to.equal(0);
  });
});

describe('HeroesCollection', () => {
  
  it('should be concattenable', () => {
    var game = new GameEngine();
    var heroes = new HeroesCollection('evens');
    heroes.setSupport(new SupportiveHero(game));
    heroes.setDefense(new DefensiveHero(game));
    var actual = heroes.ofType('Hero')
    expect(actual.length).to.equal(2);
    expect(heroes.length).to.equal(3);
    
    var concattable = heroes.concat(new Card(game));
    expect(concattable.length).to.equal(4);
  });
  
  it('should not allow pushing/popping', () => {
    var game = new GameEngine();
    var heroes = new HeroesCollection('evens');
    
    expect(() => { heroes.push(new Card(game)); }).to.throw(Error);
    expect(() => { heroes.pop(); }).to.throw(Error);
  });

  it('should allow for querying against a specific type and maintain order', () => {
    var game = new GameEngine();
    var cards = new HeroesCollection('evens');
    cards.setDefense(new DEF001BOR(game));
    cards.setOffense(new ATT001MOB(game));
    var orks = cards.ofType('Ork');
    expect(orks.length).to.equal(2);
    expect(orks[0].id).to.equal('DEF001BOR');
    expect(orks[1].id).to.equal('ATT001MOB');
  });

  it('should allow for querying for cards with not specific type', () => {
    var game = new GameEngine();
    var cards = new HeroesCollection('evens');
    cards.setDefense(new DEF001BOR(game));
    cards.setOffense(new SP001ESC(game));
    var notOrks = cards.notOfType('Ork');
    expect(notOrks.length).to.equal(1);
    expect(notOrks[0].id).to.equal('SP001ESC');
  });
  
});

describe('Hand', () => {

  it('should have a limit of 7', () => {
    var game = new GameEngine();
    var hand = new Hand();
    hand.push(new Card(game));
    hand.push(new Card(game));
    hand.push(new Card(game));
    hand.push(new Card(game));
    hand.push(new Card(game));
    hand.push(new Card(game));
    expect(hand.isFull()).to.equal(false);
    hand.push(new Card(game));
    expect(hand.isFull()).to.equal(true);
  });

});