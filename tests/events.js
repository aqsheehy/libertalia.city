import chai, { expect } from 'chai';
import EventBus from '../src/engine/events.js';

describe('EventBus', () => {

    it('should be able to publish with no handlers', () => {
        var events = new EventBus();
        events.publish('blargh', {});
    });

    it('should be able to publish and be received by multiple handlers', () => {
        var events = new EventBus();
        var result = [];
        events.subscribe('test', (data) => {
            result.push(data);
        });
        events.subscribe('test', (data) => {
            result.push(data);
        });
        var ref = {};
        events.publish('test', ref);
        expect(result.length).to.equal(2);
        expect(result[0]).to.equal(ref);
        expect(result[1]).to.equal(ref);
    });

    it('should be able to publish with no data', () => {
        var events = new EventBus();
        var result = [];
        events.subscribe('test', (data) => {
            result.push(1);
        });
        events.subscribe('test', (data) => {
            result.push(2);
        });
        var ref = {};
        events.publish('test');
        expect(result.length).to.equal(2);
        expect(result[0]).to.equal(1);
        expect(result[1]).to.equal(2);
    });
    
});