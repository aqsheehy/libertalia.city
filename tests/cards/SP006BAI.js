import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import { Card, Impediment } from '../../src/engine/card.js';
import SP006BAI from '../../src/engine/cards/SP006BAI.js';

describe('SP006BAI', () => {

    it('should move the current impediment to the enemy impediment', () => {
        var game = new GameEngine();
        var card = new SP006BAI(game);

        var impediment = new Card(game);
        game.active.impediment = impediment;

        card.play();

        expect(game.active.setting.getImpediment()).to.equal(null);
        expect(game.inactive.setting.getImpediment()).to.equal(impediment);
    });

    it('should make the current impediment attack the enemy impediment if both exists', () => {
        var game = new GameEngine();
        var card = new SP006BAI(game);
        var side = game.active;
        var enemySide = game.inactive;

        side.impediment = new Card(game);
        side.impediment.attack = function(c){ this.attacked = c;  }

        enemySide.impediment = new Card(game);        

        card.play();

        expect(side.impediment.attacked).to.equal(enemySide.impediment);
    });

    it('should play the first impediment from left to right from the opponents hand into their board', () => {
        var game = new GameEngine();
        var card = new SP006BAI(game);
        var side = game.active;
        var enemySide = game.inactive;

        var impediment = new Impediment(game);
        impediment.play = function(){ this.played = true; };
        enemySide.hand.push(impediment);

        card.play();

        expect(impediment.played).to.equal(true);
    });

});