import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import WEA001DAY from '../../src/engine/cards/WEA001DAY.js';

describe('WEA001DAY', () => {
    it('should play itself into the active weather slot on play', () => {
        var game = new GameEngine();
        var side = game.active;
        var card = new WEA001DAY(game);
        card.play();
        expect(side.setting.getWeather().id).to.equal('WEA001DAY');
    });

    it('should play the night environment on rest', () => {
        var game = new GameEngine();
        var side = game.active;
        var card = new WEA001DAY(game);
        card.rest();
        expect(side.setting.getWeather().id).to.equal('WEA002NIG');
    });
});