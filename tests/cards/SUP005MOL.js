import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import SUP005MOL from '../../src/engine/cards/SUP005MOL.js';

describe('SUP005MOL', () => {
    it('should start counting only after it has been played', () => {
        var game = new GameEngine();
        var card = new SUP005MOL(game);
        game.events.publish('play');
        card.play();
        expect(card.attributes.current.totalPlays).to.equal(0);
        game.events.publish('play');
        expect(card.attributes.current.totalPlays).to.equal(1);
    });

    it('should stop counting after it has died', () => {
        var game = new GameEngine();
        var card = new SUP005MOL(game);
        card.play();
        game.events.publish('play');
        card.death();
        game.events.publish('play');
        expect(card.attributes.current.totalPlays).to.equal(1);
    });

    it('should add 100 points if there were 20 counted plays on death', () => {
        var game = new GameEngine();
        var card = new SUP005MOL(game);
        var side = game.active;
        card.play();
        game.events.publish('play');
        game.events.publish('play');
        game.events.publish('play');
        game.events.publish('play');
        game.events.publish('play');

        game.events.publish('play');
        game.events.publish('play');
        game.events.publish('play');
        game.events.publish('play');
        game.events.publish('play');

        game.events.publish('play');
        game.events.publish('play');
        game.events.publish('play');
        game.events.publish('play');
        game.events.publish('play');

        game.events.publish('play');
        game.events.publish('play');
        game.events.publish('play');
        game.events.publish('play');
        game.events.publish('play');
        card.death();
        expect(side.points.total).to.equal(100);
    });

    it('should subtract 100 points if there were no plays counted on death', () => {
        var game = new GameEngine();
        var card = new SUP005MOL(game);
        var side = game.active;
        card.play();
        card.death();
        expect(side.points.total).to.equal(-100);
    });

    it('should subtract 100 points minus the total counted plays * 5 on death', () => {
        var game = new GameEngine();
        var card = new SUP005MOL(game);
        var side = game.active;
        card.play();
        game.events.publish('play');
        game.events.publish('play');
        game.events.publish('play');
        card.death();
        expect(side.points.total).to.equal(-85);
    });
});