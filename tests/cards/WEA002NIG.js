import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import WEA002NIG from '../../src/engine/cards/WEA002NIG.js';

describe('WEA002NIG', () => {
    it('should play itself into the active weather slot on play', () => {
        var game = new GameEngine();
        var side = game.active;
        var card = new WEA002NIG(game);
        card.play();
        expect(side.setting.getWeather().id).to.equal('WEA002NIG');
    });

    it('should play the day environment on rest', () => {
        var game = new GameEngine();
        var side = game.active;
        var card = new WEA002NIG(game);
        card.rest();
        
        expect(side.setting.getWeather().id).to.equal('WEA001DAY');
    });
});