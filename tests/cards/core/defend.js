import chai, { expect } from 'chai';
import StandardDefend from  '../../../src/engine/cards/core/defend.js';
import { Card } from  '../../../src/engine/card.js';
import GameEngine from  '../../../src/engine/game.js';

describe('StandardDefend', () => {

    it("should trigger the defending card's death if it has 0 health", () => {
        var game = new GameEngine();
        var defending = new Card(game);
        defending.attributes.modify('health', 0);
        defending.death = function(){ this.died = true };
        StandardDefend.execute(defending);
        expect(defending.died).to.equal(true);
    });

    it("should trigger the defending card's death if it has less than 0 health", () => {
        var game = new GameEngine();
        var defending = new Card(game);
        defending.attributes.modify('health', -1);
        defending.death = function(){ this.died = true };
        StandardDefend.execute(defending);
        expect(defending.died).to.equal(true);
    });

    it("should not trigger the defending card's death if it has more than 0 health", () => {
        var game = new GameEngine();
        var defending = new Card(game);
        defending.attributes.modify('health', 10);
        defending.death = function(){ this.died = true };
        StandardDefend.execute(defending);
        expect(defending.died).to.equal(undefined);
    });

    it("should not trigger the defending card's death if it has no health specified", () => {
        var game = new GameEngine();
        var defending = new Card(game);
        defending.death = function(){ this.died = true };
        StandardDefend.execute(defending);
        expect(defending.died).to.equal(undefined);
    });

});