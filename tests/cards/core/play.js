import chai, { expect } from 'chai';
import StandardPlay from  '../../../src/engine/cards/core/play.js';
import { SupportiveHero, OffensiveHero, DefensiveHero, Impediment, Environment, Weather, CombativeDecorator, Spell } from  '../../../src/engine/card.js';
import GameEngine from  '../../../src/engine/game.js';

describe('StandardPlay', () => {

    it("should deploy non-spy support heroes into the active support slot", () => {
        var game = new GameEngine();
        var card = new SupportiveHero(game);
        var side = game.active;
        StandardPlay.execute(card);
        expect(side.heroes.getSupport()).to.equal(card);
    });

    it("should deploy spy support heroes into the inactive support slot", () => {
        var game = new GameEngine();
        var card = new SpyDecorator(new SupportiveHero(game));
        var side = game.inactive;
        StandardPlay.execute(card);
        expect(side.heroes.getSupport()).to.equal(card);
    });

    it("should deploy non-spy offense heroes into the active offense slot", () => {
        var game = new GameEngine();
        var card = new OffensiveHero(game);
        var side = game.active;
        StandardPlay.execute(card);
        expect(side.heroes.getOffense()).to.equal(card);
    });

    it("should deploy spy offense heroes into the inactive offense slot", () => {
        var game = new GameEngine();
        var card = new SpyDecorator(new OffensiveHero(game));
        var side = game.inactive;
        StandardPlay.execute(card);
        expect(side.heroes.getOffense()).to.equal(card);
    });

    it("should deploy non-spy defense heroes into the active defense slot", () => {
        var game = new GameEngine();
        var card = new DefensiveHero(game);
        var side = game.active;
        StandardPlay.execute(card);
        expect(side.heroes.getDefense()).to.equal(card);
    });

    it("should deploy spy defense heroes into the inactive defense slot", () => {
        var game = new GameEngine();
        var card = new SpyDecorator(new DefensiveHero(game));
        var side = game.inactive;
        StandardPlay.execute(card);
        expect(side.heroes.getDefense()).to.equal(card);
    });

    it("should deploy non-spy impediments into the active impediment slot", () => {
        var game = new GameEngine();
        var card = new Impediment(game);
        var side = game.active;
        StandardPlay.execute(card);
        expect(side.setting.getImpediment()).to.equal(card);
    });

    it("should deploy spy impediments into the inactive impediment slot", () => {
        var game = new GameEngine();
        var card = new SpyDecorator(new Impediment(game));
        var side = game.inactive;
        StandardPlay.execute(card);
        expect(side.setting.getImpediment()).to.equal(card);
    });

    it("should deploy non-spy environments into the active environment slot", () => {
        var game = new GameEngine();
        var card = new Environment(game);
        var side = game.active;
        StandardPlay.execute(card);
        expect(side.setting.getEnvironment()).to.equal(card);
    });

    it("should deploy spy environments into the active environment slot", () => {
        var game = new GameEngine();
        var card = new SpyDecorator(new Environment(game));
        var side = game.inactive;
        StandardPlay.execute(card);
        expect(side.setting.getEnvironment()).to.equal(card);
    });

    it("should deploy non-spy weathers into the active weather slot", () => {
        var game = new GameEngine();
        var card = new Weather(game);
        var side = game.active;
        StandardPlay.execute(card);
        expect(side.setting.getWeather()).to.equal(card);
    });

    it("should deploy spy weathers into the inactive weather slot", () => {
        var game = new GameEngine();
        var card = new SpyDecorator(new Weather(game));
        var side = game.inactive;
        StandardPlay.execute(card);
        expect(side.setting.getWeather()).to.equal(card);
    });

    it("should deploy non-spy spells into the active graveyard", () => {
        var game = new GameEngine();
        var card = new Spell(game);
        StandardPlay.execute(card);
        expect(game.active.grave[0]).to.equal(card);
    });

    it("should deploy spy spells into the inactive graveyard", () => {
        var game = new GameEngine();
        var card = new SpyDecorator(new Spell(game));
        StandardPlay.execute(card);
        expect(game.inactive.grave[0]).to.equal(card);
    });


});

class SpyDecorator extends CombativeDecorator {
    get types(){ return ['Spy'].concat(super.types); } 
}