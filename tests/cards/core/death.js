import chai, { expect } from 'chai';
import StandardDeath from '../../../src/engine/cards/core/death.js';
import { Card } from '../../../src/engine/card.js';
import GameEngine from '../../../src/engine/game.js';

describe('StandardDeath', () => {
    it('should send evens cards to the evens graveyard', () => {
        var game = new GameEngine();
        var card = new Card(game);
        game.active.impediment = card;
        StandardDeath.death(card);
        expect(game.active.grave[0]).to.equal(card);
    });

    it('should send odds cards to the odds graveyard', () => {
        var game = new GameEngine();
        var card = new Card(game);
        game.inactive.impediment = card;
        StandardDeath.death(card);
        expect(game.inactive.grave[0]).to.equal(card);
    });
});