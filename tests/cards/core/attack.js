import chai, { expect } from 'chai';
import StandardAttack from  '../../../src/engine/cards/core/attack.js';
import { Card } from  '../../../src/engine/card.js';
import GameEngine from  '../../../src/engine/game.js';

describe('StandardAttack', () => {

    it("should subtract the source's attack from the dest's health, and the dest should defend", () => {
        var game = new GameEngine();

        var source = new Card(game);
        source.attributes.add('damage', 4);

        var dest = new Card(game);
        dest.defend = function(card) { this.defendedFrom = card; };
        dest.attributes.add('health', 5);
        
        StandardAttack.execute(source, dest);
        expect(dest.attributes.current.health).to.equal(1);
        expect(dest.defendedFrom).to.equal(source);
    });

    it('should do nothing if the source has no attack field', () => {
        var game = new GameEngine();

        var source = new Card(game);
        var dest = new Card(game);
        dest.defend = function(card) { this.defendedFrom = card; };
        dest.attributes.add('health', 5);
        
        StandardAttack.execute(source, dest);
        expect(dest.attributes.current.health).to.equal(5);
        expect(dest.defendedFrom).to.equal(undefined);
    });

    it('should do nothing if the destination has no health field', () => {
        var game = new GameEngine();

        var source = new Card(game);
        source.attributes.add('damage', 4);

        var dest = new Card(game);
        dest.defend = function(card) { this.defendedFrom = card; };
        
        StandardAttack.execute(source, dest);
        expect(dest.defendedFrom).to.equal(undefined);
    });
    
});