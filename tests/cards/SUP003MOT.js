import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import SUP003MOT from '../../src/engine/cards/SUP003MOT.js';

describe('SUP003MOT', () => {
    it('should start counting only after it has been played', () => {
        var game = new GameEngine();
        var card = new SUP003MOT(game);
        game.events.publish('victory');
        card.play();
        expect(card.attributes.current.totalVictories).to.equal(0);
        game.events.publish('victory');
        expect(card.attributes.current.totalVictories).to.equal(1);
    });

    it('should stop counting after it has died', () => {
        var game = new GameEngine();
        var card = new SUP003MOT(game);
        card.play();
        game.events.publish('victory');
        card.death();
        game.events.publish('victory');
        expect(card.attributes.current.totalVictories).to.equal(1);
    });

    it('should add 100 points if there were 10 counted victories on death', () => {
        var game = new GameEngine();
        var card = new SUP003MOT(game);
        var side = game.active;
        card.play();
        game.events.publish('victory');
        game.events.publish('victory');
        game.events.publish('victory');
        game.events.publish('victory');
        game.events.publish('victory');
        game.events.publish('victory');
        game.events.publish('victory');
        game.events.publish('victory');
        game.events.publish('victory');
        game.events.publish('victory');
        card.death();
        expect(side.points.total).to.equal(100);
    });

    it('should subtract 100 points if there were no victories counted on death', () => {
        var game = new GameEngine();
        var card = new SUP003MOT(game);
        var side = game.active;
        card.play();
        card.death();
        expect(side.points.total).to.equal(-100);
    });

    it('should subtract 100 points minus the total counted victories * 10 on death', () => {
        var game = new GameEngine();
        var card = new SUP003MOT(game);
        var side = game.active;
        card.play();
        game.events.publish('victory');
        game.events.publish('victory');
        game.events.publish('victory');
        card.death();
        expect(side.points.total).to.equal(-70);
    });
});