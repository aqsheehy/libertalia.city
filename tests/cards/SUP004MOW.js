import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import SUP004MOW from '../../src/engine/cards/SUP004MOW.js';

describe('SUP004MOW', () => {
    it('should start counting only after it has been played', () => {
        var game = new GameEngine();
        var card = new SUP004MOW(game);
        game.events.publish('discard');
        card.play();
        expect(card.attributes.current.totalDiscards).to.equal(0);
        game.events.publish('discard');
        expect(card.attributes.current.totalDiscards).to.equal(1);
    });

    it('should stop counting after it has died', () => {
        var game = new GameEngine();
        var card = new SUP004MOW(game);
        card.play();
        game.events.publish('discard');
        card.death();
        game.events.publish('discard');
        expect(card.attributes.current.totalDiscards).to.equal(1);
    });

    it('should add 100 points if there were 5 counted discards on death', () => {
        var game = new GameEngine();
        var card = new SUP004MOW(game);
        var side = game.active;
        card.play();
        game.events.publish('discard');
        game.events.publish('discard');
        game.events.publish('discard');
        game.events.publish('discard');
        game.events.publish('discard');
        card.death();
        expect(side.points.total).to.equal(100);
    });

    it('should subtract 100 points if there were no discards counted on death', () => {
        var game = new GameEngine();
        var card = new SUP004MOW(game);
        var side = game.active;
        card.play();
        card.death();
        expect(side.points.total).to.equal(-100);
    });

    it('should subtract 100 points minus the total counted discards * 20 on death', () => {
        var game = new GameEngine();
        var card = new SUP004MOW(game);
        var side = game.active;
        card.play();
        game.events.publish('discard');
        game.events.publish('discard');
        game.events.publish('discard');
        card.death();
        expect(side.points.total).to.equal(-40);
    });
});