import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import SUP006BNK from '../../src/engine/cards/SUP006BNK.js';

describe('SUP006BNK', () => {

    it('should loan 1/2 of the active points and initialize interest settings on play', () => {
        var game = new GameEngine();
        var card = new SUP006BNK(game);
        var active = game.active;
        active.points.add(100);
        var inactive = game.inactive;
        card.play();
        expect(active.points.total).to.equal(50);
        expect(card.iterations).to.equal(0);
        expect(card.interest).to.equal(34);
    });

    it('should do nothing if the points are 0s', () => {
        var game = new GameEngine();
        var card = new SUP006BNK(game);
        var active = game.active;
        var inactive = game.inactive;
        card.play();
        expect(active.points.total).to.equal(0);
        expect(card.iterations).to.equal(0);
        expect(card.interest).to.equal(0);
    });

    it('should add interest to active side and subtract interest from inactive hand', () => {
        var game = new GameEngine();
        var card = new SUP006BNK(game);
        var active = game.active;
        var inactive = game.inactive;
        card.interest = 100;
        card.applyInterest();
        expect(active.points.total).to.equal(100);
        expect(inactive.points.total).to.equal(-100);
    });

    it('should re-initiate the loan if it becomes restless', () => {
        var game = new GameEngine();
        var card = new SUP006BNK(game);
        var active = game.active;
        var inactive = game.inactive;
        card.loan = function(){ this.loaned = true; }
        card.idle();
        card.idle();
        expect(card.loaned).to.equal(true);
    });

    it('shouldnt only apply interest if the loan hasnt been initiated', () => {
        var game = new GameEngine();
        var card = new SUP006BNK(game);
        var active = game.active;
        var inactive = game.inactive;
        card.loanInitiated = false;
        card.interestApplied = 0;
        card.applyInterest = function(){ this.interestApplied++; }
        card.rest();
        expect(card.interestApplied).to.equal(0);
    });

    it('shouldnt only apply interest three times', () => {
        var game = new GameEngine();
        var card = new SUP006BNK(game);
        var active = game.active;
        var inactive = game.inactive;
        card.loanInitiated = true;
        card.interestApplied = 0;
        card.applyInterest = function(){ this.interestApplied++; }
        card.rest();
        card.rest();
        card.rest();
        card.rest();
        expect(card.interestApplied).to.equal(3);
    });

});