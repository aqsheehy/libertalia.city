import chai, { expect } from 'chai';
import RestingCombatLogic from '../../../src/engine/cards/combat/rest.js';
import { Card } from '../../../src/engine/card.js';
import GameEngine from '../../../src/engine/game.js';

describe('RestingCombatLogic', () => {
    it('should rest all active heroes and settings ', () => {
        var game = new GameEngine();
        var combat = new RestingCombatLogic();
        var side = game.active;
        var rests = 0;

        var offense = new Card(game);
        offense.rest = function() { rests++; }
        side.offense = offense;

        var defense = new Card(game);
        defense.rest = function() { rests++; }
        side.defense = defense;

        var support = new Card(game);
        support.rest = function() { rests++; }
        side.support = support;
        
        var impediment = new Card(game);
        impediment.rest = function() { rests++; }
        side.impediment = impediment;

        var weather = new Card(game);
        weather.rest = function() { rests++; }
        side.weather = weather;
        
        var environment = new Card(game);
        environment.rest = function() { rests++; }
        side.environment = environment;

        combat.execute(side);
        expect(rests).to.equal(6);
    });
});