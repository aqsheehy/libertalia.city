import chai, { expect } from 'chai';
import MirrorCombatLogic from '../../../src/engine/cards/combat/mirror.js';
import { Card } from '../../../src/engine/card.js';
import GameEngine from '../../../src/engine/game.js';

describe('MirrorCombatLogic', () => {

    it('should not do anything if the current impediment is not active', () => {
        var game = new GameEngine();
        var combat = new MirrorCombatLogic();
        var active = game.active;
        var inactive = game.inactive;

        var activeOffense = new Card(game);
        activeOffense.attack = function(c) { this.attacked = c; }
        active.offense = activeOffense;
        
        var activeDefense = new Card(game);
        activeDefense.attack = function(c) { this.attacked = c; }
        active.defense = activeDefense;

        var activeSupport = new Card(game);
        activeSupport.attack = function(c) { this.attacked = c; }
        active.support = activeSupport;

        var mirror = new Card(game);

        var inactiveOffense = new Card(game);
        inactiveOffense.attack = function(c) { this.attacked = c; }
        inactive.offense = inactiveOffense;

        var inactiveDefense = new Card(game);
        inactiveDefense.attack = function(c) { this.attacked = c; }
        inactive.defense = inactiveDefense;

        var inactiveSupport = new Card(game);
        inactiveSupport.attack = function(c) { this.attacked = c; }
        inactive.support = inactiveSupport;

        combat.execute(active, inactive);

        expect(activeOffense.attacked).to.equal(undefined);
        expect(activeDefense.attacked).to.equal(undefined);
        expect(activeSupport.attacked).to.equal(undefined);

        expect(inactiveOffense.attacked).to.equal(undefined);
        expect(inactiveDefense.attacked).to.equal(undefined);
        expect(inactiveSupport.attacked).to.equal(undefined);
    });

    it('should make heroes attack the opposite hero', () => {

        var game = new GameEngine();
        var combat = new MirrorCombatLogic();
        var active = game.active;
        var inactive = game.inactive;

        var activeOffense = new Card(game);
        activeOffense.attack = function(c) { this.attacked = c; }
        active.offense = activeOffense;

        var activeDefense = new Card(game);
        activeDefense.attack = function(c) { this.attacked = c; }
        active.defense = activeDefense;

        var activeSupport = new Card(game);
        activeSupport.attack = function(c) { this.attacked = c; }
        active.support = activeSupport;

        active.impediment = new Card(game)

        var inactiveOffense = new Card(game);
        inactiveOffense.attack = function(c) { this.attacked = c; }
        inactive.offense = inactiveOffense;

        var inactiveDefense = new Card(game);
        inactiveDefense.attack = function(c) { this.attacked = c; }
        inactive.defense = inactiveDefense;

        var inactiveSupport = new Card(game);
        inactiveSupport.attack = function(c) { this.attacked = c; }
        inactive.support = inactiveSupport;

        combat.execute(active, inactive);

        expect(activeOffense.attacked).to.equal(inactiveOffense);
        expect(activeDefense.attacked).to.equal(inactiveDefense);
        expect(activeSupport.attacked).to.equal(inactiveSupport);

        expect(inactiveOffense.attacked).to.equal(active.impediment);
        expect(inactiveDefense.attacked).to.equal(active.impediment);
        expect(inactiveSupport.attacked).to.equal(active.impediment);

    });
});