import chai, { expect } from 'chai';
import StandardCombatLogic from '../../../src/engine/cards/combat/standard.js';
import { Combative } from '../../../src/engine/card.js';
import GameEngine from '../../../src/engine/game.js';

describe('StandardCombatLogic', () => {

    it('should rest all heroes and settings still alive at the end of combat', () => {
        var game = new GameEngine();
        var combat = new StandardCombatLogic();
        var side = game.active;
        var rests = 0;

        var offense = new Combative(game);
        offense.rest = function() { rests++; }
        side.offense = offense;

        var defense = new Combative(game);
        defense.rest = function() { rests++; }
        side.defense = defense;

        var support = new Combative(game);
        support.rest = function() { rests++; }
        side.support = support;
        
        var impediment = new Combative(game);
        impediment.rest = function() { rests++; }
        side.impediment = impediment;

        var weather = new Combative(game);
        weather.rest = function() { rests++; }
        side.weather = weather;
        
        var environment = new Combative(game);
        environment.rest = function() { rests++; }
        side.environment = environment;

        combat.execute(side);
        expect(rests).to.equal(6);
    });

    it('should idle all heroes if the impediment is inactive (dead)', () => {
        var game = new GameEngine();
        var combat = new StandardCombatLogic();
        var side = game.active;
        var idles = 0;

        var offense = new Combative(game);
        offense.idle = function() { idles++; }
        side.offense = offense;

        var defense = new Combative(game);
        defense.idle = function() { idles++; }
        side.defense = defense;

        var support = new Combative(game);
        support.idle = function() { idles++; }
        side.support = support;
        
        var impediment = new Combative(game);
        impediment.idle = function() { idles++; }
        side.impediment = impediment;

        impediment.death();

        combat.execute(side);
        expect(idles).to.equal(3);
    });

    it('should idle the impediment if all heroes are inactive', () => {
        var game = new GameEngine();
        var combat = new StandardCombatLogic();
        var side = game.active;

        var idles = 0;

        var offense = new Combative(game);
        offense.idle = function() { idles++; }
        side.offense = offense;

        var defense = new Combative(game);
        defense.idle = function() { idles++; }
        side.defense = defense;

        var support = new Combative(game);
        support.idle = function() { idles++; }
        side.support = support;
        
        var impediment = new Combative(game);
        impediment.idle = function() { this.idled = true; }
        side.impediment = impediment;

        side.heroes.forEach(c => c.death());

        combat.execute(side);

        expect(idles).to.equal(0);
        expect(impediment.idled).to.equal(true);
    });

    it('should trigger victory if heroes manage to kill the impediment', () => {
        var game = new GameEngine();
        var combat = new StandardCombatLogic();
        var side = game.active;
        var counter = 0;

        var offense = new Combative(game);
        offense.attack = function(target) { 
            counter++; if (counter == 3) target.death();
        };
        offense.victory = function(){ counter++;};

        var defense = new Combative(game);
        defense.attack = function(target) { 
            counter++; if (counter == 3) target.death();
        };
        defense.victory = function(){ counter++;};

        var support = new Combative(game);
        support.attack = function(target) { 
            counter++; if (counter == 3) target.death();
        };
        support.victory = function(){ counter++;};

        side.offense = offense;
        side.defense = defense;
        side.support = support;

        var impediment = new Combative(game);
        side.impediment = impediment;

        combat.execute(side);
        expect(counter).to.equal(6);
    });

    it('should have the impediment attack all heroes if there is no defense', () => {
        var game = new GameEngine();
        var combat = new StandardCombatLogic();
        var side = game.active;

        side.offense = new Combative(game);
        side.support = new Combative(game);

        var impediment = new Combative(game);
        side.impediment = impediment;
        impediment.counter = 0;
        impediment.attack = function(){ this.counter++; }

        combat.execute(side);
        expect(impediment.counter).to.equal(2);
    });

    it('should trigger the victory condition of the impediment if it kills all heroes', () => {
        var game = new GameEngine();
        var combat = new StandardCombatLogic();
        var side = game.active;

        var offense = new Combative(game);
        side.offense = offense;
        var support = new Combative(game);
        side.support = support;

        var impediment = new Combative(game);
        var order = [];
        side.impediment = impediment;
        impediment.attack = function(target){ 
            target.death() 
            order.push(target);
        }
        impediment.victory = function(){ this.victorious = true; }

        combat.execute(side);
        expect(impediment.victorious).to.equal(true);
        expect(order.length).to.equal(2);
        expect(order[0]).to.equal(offense);
        expect(order[1]).to.equal(support);
    });

    it('should prevent victory if there is a defense hero, as only the defensive hero will be attacked', () => {
        var game = new GameEngine();
        var combat = new StandardCombatLogic();
        var side = game.active;

        var defense = new Combative(game);
        side.defense = defense;
        var offense = new Combative(game);
        side.offense = offense;
        var support = new Combative(game);
        side.support = support;

        var impediment = new Combative(game);
        var order = [];
        side.impediment = impediment;
        impediment.attack = function(target){ 
            target.death() 
            order.push(target);
        }
        impediment.victory = function(){ this.victorious = true; }

        combat.execute(side);
        expect(impediment.victorious).to.equal(undefined);
        expect(order.length).to.equal(1);
        expect(order[0]).to.equal(defense);
    });

    it('should trigger victory if there is ONLY a defense hero, as only the defensive hero will be the only one killed', () => {
        var game = new GameEngine();
        var combat = new StandardCombatLogic();
        var side = game.active;

        var defense = new Combative(game);
        side.defense = defense;

        side.impediment = new Combative(game);

        var order = [];
        side.impediment.attack = function(target){ 
            target.death() 
            order.push(target);
        }
        side.impediment.victory = function(){ this.victorious = true; }

        combat.execute(side);
        expect(side.impediment.victorious).to.equal(true);
        expect(order.length).to.equal(1);
        expect(order[0]).to.equal(defense);
    });
});