import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import IMP008MQN, { ATTRIBUTES, TAX_INCREMENT, TAX_DISTRIBUTION } from '../../src/engine/cards/IMP008MQN.js';
import { Card } from '../../src/engine/card.js';

describe('IMP008MQN', () => {
    it('should only return points available on either side, but subtract the full from the total', () => {
        var game = new GameEngine();
        var side = game.active;
        var card = new IMP008MQN(game);
        side.points.add(TAX_INCREMENT + 10);
        expect(card.tax(side)).to.equal(TAX_INCREMENT);
        expect(side.points.total).to.equal(10);
        expect(card.tax(side)).to.equal(10);
        expect(side.points.total).to.equal(-(TAX_INCREMENT - 10));
    });

    it('should distribute tax equally to the bounty of all heroes on the board', () => {
        var game = new GameEngine();
        var side = game.active;
        side.heroes[0] = new Card(game);
        side.heroes[1] = new Card(game);
        side.heroes[2] = new Card(game);
        var card = new IMP008MQN(game);
        card.distributeTax(side, TAX_INCREMENT);
        side.heroes.forEach((card) => card.death());
        expect(side.points.total).to.equal(TAX_INCREMENT);
    });

    it('should cause all heroes to become restless, and the mad queen should kill them afterwards', () => {
        var game = new GameEngine();
        var side = game.active;
        side.heroes[0] = new Card(game);
        side.heroes[1] = new Card(game);
        side.heroes[2] = new Card(game);
        var card = new IMP008MQN(game);
        card.distributeTax(side, TAX_INCREMENT);
        card.distributeTax(side, 0);
        card.distributeTax(side, 0);
        expect(side.points.total).to.equal(TAX_INCREMENT);
    });

    it('should add the remainder of un-distributed taxes to the queen bounty', () => {
        var game = new GameEngine();
        var side = game.active;
        side.heroes[0] = new Card(game);
        side.heroes[1] = new Card(game);
        var card = new IMP008MQN(game);
        card.distributeTax(side, TAX_INCREMENT);
        side.heroes.filter(c => !!c).forEach((c) => c.death());
        expect(side.points.total).to.equal(TAX_INCREMENT / 3 * 2);
        side.impediment = card;
        card.death();
        expect(side.points.total).to.equal(TAX_INCREMENT);
    });
});
