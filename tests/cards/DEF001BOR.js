import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import { Card } from '../../src/engine/card.js';
import DEF001BOR, { ATTRIBUTES } from '../../src/engine/cards/DEF001BOR.js';

describe('DEF001BOR', () => {
    it('should increase its damage and health by the count of orks on play', () => {
        var game = new GameEngine();
        var card = new DEF001BOR(game);
        var otherOrk = new Ork(game);
        var side = game.active;

        side.defense = card;
        side.offense = otherOrk;
        card.play();

        expect(card.attributes.current.damage).to.equal(ATTRIBUTES.damage + 2);
        expect(card.attributes.current.health).to.equal(ATTRIBUTES.health + 2);
    });

    it('should increase its damage by 1 every time it defends', () => {
        var game = new GameEngine();
        var card = new DEF001BOR(game);
        card.defend(new Card(game));

        expect(card.attributes.current.damage).to.equal(ATTRIBUTES.damage + 1);
        expect(card.attributes.current.health).to.equal(ATTRIBUTES.health);
    });

    it('should increase its damage by 1 every time it attacks', () => {
        var game = new GameEngine();
        var card = new DEF001BOR(game);
        card.attack(new Card(game));

        expect(card.attributes.current.damage).to.equal(ATTRIBUTES.damage + 1);
        expect(card.attributes.current.health).to.equal(ATTRIBUTES.health);
    });

    it('should increase the damage of all other ork heroes by 1 when it kills an enemy', () => {
        var game = new GameEngine();
        var card = new DEF001BOR(game);
        var side = game.active;
        var otherOrk = new Ork(game);
        game.active.defense = card;
        game.active.offense = otherOrk;
        card.kill(new Card(game));

        expect(card.attributes.current.damage).to.equal(ATTRIBUTES.damage + 1);
        expect(card.attributes.current.health).to.equal(ATTRIBUTES.health);

        expect(otherOrk.attributes.current.damage).to.equal(1);
        expect(otherOrk.attributes.current.health).to.equal(undefined);
    });

    it('should become restless on one idle, and should no longer be restless on victory', () => {
        var game = new GameEngine();
        var card = new DEF001BOR(game);

        card.idle();
        expect(card.attributes.current.restless).to.equal(true);

        card.victory();
        expect(card.attributes.current.restless).to.equal(false);
    });

    it('on a second consecutive idle, it should become the enemy defense if there is none', () => {
        var game = new GameEngine();
        var card = new DEF001BOR(game);
        game.active.defense = card;

        card.idle();
        card.idle();

        expect(game.active.defense).to.equal(null);
        expect(game.inactive.defense).to.equal(card);
    });

    it('on a second consecutive idle, it should attack the enemy defense and become its place if there is one and it dies', () => {
        var game = new GameEngine();
        var card = new DEF001BOR(game);
        card.attack = function(c) { this.attacked = true; c.death(); }
        game.active.defense = card;

        var enemyDefense = new Card(game);
        enemyDefense.attack = function(){ this.attacked = true; }
        game.inactive.defense = enemyDefense;

        card.idle();
        card.idle();

        expect(card.attacked).to.equal(true);
        expect(enemyDefense.attacked).to.equal(undefined);
        expect(game.active.defense).to.equal(null);
        expect(game.inactive.defense).to.equal(card);
    });

    it('on a second consecutive idle, it should attack the enemy defense. If it lives, it should attack back', () => {
        var game = new GameEngine();
        var card = new DEF001BOR(game);
        card.attack = function(c) { this.attacked = true; }
        var side = game.active;
        side.defense = card;
        var enemySide = game.inactive;
        var enemyDefense = new Card(game);
        enemyDefense.attack = function(){ this.attacked = true; }
        enemySide.defense = enemyDefense;

        card.idle();
        card.idle();

        expect(card.attacked).to.equal(true);
        expect(enemyDefense.attacked).to.equal(true);
        expect(side.defense).to.equal(card);
        expect(enemySide.defense).to.equal(enemyDefense);
    });
});

class Ork extends Card {
    get types(){ return ['Ork', 'Hero', 'Card']; }
}