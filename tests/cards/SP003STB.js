import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import SP003STB from '../../src/engine/cards/SP003STB.js';
import DEF001BOR from '../../src/engine/cards/DEF001BOR.js';

describe('SP003STB', () => {
        
    it('should have no effect if there is no defense card in play', () => {
        
        var game = new GameEngine();
        var side = game.active;
        var card = new SP003STB(game);
        card.play();
        
        expect(side.heroes.ofType('Defensive').length).to.equal(0);        
    });
    
    it('should stealth the defensive hero, until the end of the current turn', () => {
        
        var game = new GameEngine();
        var side = game.active;
        side.defense = new DEF001BOR(game);
                
        var card = new SP003STB(game);
        card.play();
        
        var defense = side.heroes.getDefense();
        
        expect(defense.isStealthed()).to.equal(true);
        expect(defense.active).to.equal(false);
        
        game.events.publish('finished');

        expect(defense.isStealthed()).to.equal(false);
        expect(defense.active).to.equal(true);
    });
    
});