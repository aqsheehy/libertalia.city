import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import { Card } from '../../src/engine/card.js';
import ATT005TRV from '../../src/engine/cards/ATT005TRV.js';

describe('ATT005TRV', () => {
    it('should be able to do a full survey of the board and end up where it started', () => {
        var game = new GameEngine();
        var card = new ATT005TRV(game);
        card.play();

        var rests = 0;
        do card.rest()
        while (rests++ < 5);

        expect(game.active.points.total).to.equal(200);
    });

    it('should penalize the active player if it cannot move', () => {
        var game = new GameEngine();
        var card = new ATT005TRV(game);
        card.play();

        game.active.defense = new Card(game);
        game.active.support = new Card(game);

        card.rest();

        expect(game.active.points.total).to.equal(-10);
    });

    it('should go to the opposite support slot if its defensive and the support is open', () => {
        var game = new GameEngine();
        var card = new ATT005TRV(game);
        
        game.active.defense = card;
        
        card.rest();

        expect(game.inactive.support).to.equal(card);
    });

    it('should go to the home offense slot if its defensive and the support is blocked', () => {
        var game = new GameEngine();
        var card = new ATT005TRV(game);
        
        game.active.defense = card;
        game.inactive.support = new Card(game);
        card.rest();

        expect(game.active.offense).to.equal(card);
    });

    it('should go to the home defense slot if its offensive and the defense is open', () => {
        var game = new GameEngine();
        var card = new ATT005TRV(game);
        
        game.active.offense = card;
        
        card.rest();

        expect(game.active.defense).to.equal(card);
    });

    it('should go to the home support slot if its offensive and the defense is blocked', () => {
        var game = new GameEngine();
        var card = new ATT005TRV(game);
        
        game.active.defense = new Card(game);
        game.active.offense = card;
        card.rest();

        expect(game.active.support).to.equal(card);
    });

    it('should go to the home offense slot if its supportive and the offense is open', () => {
        var game = new GameEngine();
        var card = new ATT005TRV(game);
        
        game.active.support = card;
        
        card.rest();

        expect(game.active.offense).to.equal(card);
    });

    it('should go to the opposite defense slot if its supportive and the offense is blocked', () => {
        var game = new GameEngine();
        var card = new ATT005TRV(game);
        
        game.active.offense = new Card(game);
        game.active.support = card;
        
        card.rest();

        expect(game.inactive.defense).to.equal(card);
    });
});