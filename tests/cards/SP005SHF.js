import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import { Card } from '../../src/engine/card.js';
import { HeroesCollection } from '../../src/engine/cards.js';
import SP005SHF from '../../src/engine/cards/SP005SHF.js';

describe('SP005SHF', () => {

    it('should shuffle both the active and inactive side', () => {
        var game = new GameEngine();
        var card = new SP005SHF(game);
        card.shuffleSide = function(heroes) { heroes.shuffled = true; };
        card.play();
        expect(game.active.shuffled).to.equal(true);
        expect(game.inactive.shuffled).to.equal(true);
    });

    it('should shuffle all heroes to the left, wrapping the left most card to the back', () => {
        var game = new GameEngine();
        var card = new SP005SHF(game);

        game.active.defense = new CardWithId(game, 'DEFENSE');
        game.active.offense = new CardWithId(game, 'OFFENSE');
        game.active.support = new CardWithId(game, 'SUPPORT');

        card.shuffleSide(game.active);
        
        expect(game.active.support.id).to.equal('DEFENSE');
        expect(game.active.defense.id).to.equal('OFFENSE');
        expect(game.active.offense.id).to.equal('SUPPORT');
    });
    
});

class CardWithId extends Card {
    constructor(game, id){
        super(game);
        this.$id = id;
    }

    get id(){ return this.$id; }
}