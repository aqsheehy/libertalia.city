import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import { Card } from '../../src/engine/card.js';
import DEF002OBC, { ATTRIBUTES } from '../../src/engine/cards/DEF002OBC.js';

describe('DEF002OBC', () => {
    it('should be keeping stock of all deaths', () => {
        var game = new GameEngine();
        var card = new DEF002OBC(game);
        game.events.publish('death', {});
        expect(card.attributes.current.totalDeaths).to.equal(1);
    });

    it('should increment its health by the # of deaths', () => {
        var game = new GameEngine();
        var card = new DEF002OBC(game);
        var side = game.active;
        game.events.publish('death', {});
        card.play();
        var attr = card.attributes.current;
        expect(attr.health).to.equal(ATTRIBUTES.health + 1);
        expect(side.points.total).to.equal(0);
    });

    it('should increment its health by the # of deaths, and gain 2 points per death afterwards', () => {
        var game = new GameEngine();
        var card = new DEF002OBC(game);
        var side = game.active;
        game.events.publish('death', {});
        card.play();
        var attr = card.attributes.current;
        expect(attr.health).to.equal(ATTRIBUTES.health + 1);
        expect(side.points.total).to.equal(0);
        game.events.publish('death', {});
        expect(side.points.total).to.equal(2);
    });

    it('should decrement the points by 10 every time it idles', () => {
        var game = new GameEngine();
        var card = new DEF002OBC(game);
        var side = game.active;
        card.idle()
        expect(side.points.total).to.equal(-4);
    });
});