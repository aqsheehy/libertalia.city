import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import { Card, DefensiveHero } from '../../src/engine/card.js';
import IMP001BAR from '../../src/engine/cards/IMP001BAR.js';
import IMP007MOR from '../../src/engine/cards/IMP007MOR.js';

describe('IMP001BAR', () => {
    it('should decrement the active sides points by 100 if the roc is victorious', () => {
        var game = new GameEngine();
        var side = game.active;
        var card = new IMP001BAR(game);
        card.victory();
        expect(side.points.total).to.equal(-100);
    });

    it('should play the first hero from hand and deploy its mother on death', () => {
        var game = new GameEngine();
        var side = game.active;
        var toPlay = new DefensiveHero(game);
        toPlay.play = function() { this.played = true };
        side.hand.push(toPlay);

        var card = new IMP001BAR(game);
        game.active.impediment = card;
        card.death();

        expect(toPlay.played).to.equal(true);
        expect(side.setting.getImpediment().id).to.equal('IMP007MOR');
    });
});

describe('IMP007MOR', () => {
    it('should decrement the active sides points by 20 if the roc mother is victorious', () => {
        var game = new GameEngine();
        var side = game.active;
        var card = new IMP007MOR(game);
        card.victory();
        expect(side.points.total).to.equal(-20);
    });
});