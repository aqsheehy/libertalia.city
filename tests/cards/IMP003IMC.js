import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import { Card, DefensiveHero } from '../../src/engine/card.js';
import IMP003IMC from '../../src/engine/cards/IMP003IMC.js';

describe('IMP003IMC', () => {
    it('should play the first hero from the hand on this side when deployed', () => {
        var game = new GameEngine();
        var card = new IMP003IMC(game);
        var side = game.active;
        var toPlay = new DefensiveHero(game);
        toPlay.play = function(){ this.played = true };
        side.hand.push(toPlay)
        card.play();     
        expect(toPlay.played).to.equal(true);
    });

    it('should play the first hero from the hand on this side when idle', () => {
        var game = new GameEngine();
        var card = new IMP003IMC(game);
        var side = game.active;
        var toPlay = new DefensiveHero(game);
        toPlay.play = function(){ this.played = true };
        side.hand.push(toPlay)
        card.idle();     
        expect(toPlay.played).to.equal(true);
    });

    it('should play the first hero from the hand on this side when victorious', () => {
        var game = new GameEngine();
        var card = new IMP003IMC(game);
        var side = game.active;
        var toPlay = new DefensiveHero(game);
        toPlay.play = function(){ this.played = true };
        side.hand.push(toPlay)
        card.idle();     
        expect(toPlay.played).to.equal(true);
    });

    it('should decrement the points of the current side by 50 when victorious', () => {
        var game = new GameEngine();
        var card = new IMP003IMC(game);
        var side = game.active;
        card.victory();     
        expect(side.points.total).to.equal(-50);
    });
});