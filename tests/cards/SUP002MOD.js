import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import SUP002MOD from '../../src/engine/cards/SUP002MOD.js';

describe('SUP002MOD', () => {
    it('should start counting only after it has been deployed', () => {
        var game = new GameEngine();
        var card = new SUP002MOD(game);
        game.events.publish('death');
        card.play();
        expect(card.attributes.current.totalDeaths).to.equal(0);
        game.events.publish('death');
        expect(card.attributes.current.totalDeaths).to.equal(1);
    });

    it('should stop counting after it has died', () => {
        var game = new GameEngine();
        var card = new SUP002MOD(game);
        card.play();
        game.events.publish('death');
        card.death();
        game.events.publish('death');
        expect(card.attributes.current.totalDeaths).to.equal(1);
    });

    it('should add 100 points if there were 5 counted deaths on death', () => {
        var game = new GameEngine();
        var card = new SUP002MOD(game);
        var side = game.active;
        card.play();
        game.events.publish('death');
        game.events.publish('death');
        game.events.publish('death');
        game.events.publish('death');
        game.events.publish('death');
        card.death();
        expect(side.points.total).to.equal(100);
    });

    it('should subtract 100 points if there were no deaths counted on death', () => {
        var game = new GameEngine();
        var card = new SUP002MOD(game);
        var side = game.active;
        card.play();
        card.death();
        expect(side.points.total).to.equal(-100);
    });

    it('should subtract 100 points minus the total counted deaths * 20 on death', () => {
        var game = new GameEngine();
        var card = new SUP002MOD(game);
        var side = game.active;
        card.play();
        game.events.publish('death');
        game.events.publish('death');
        game.events.publish('death');
        card.death();
        expect(side.points.total).to.equal(-40);
    });
});