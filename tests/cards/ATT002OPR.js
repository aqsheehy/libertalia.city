import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import { Card, DefensiveHero } from '../../src/engine/card.js';
import ATT002OPR from '../../src/engine/cards/ATT002OPR.js';
import WEA003ORF from '../../src/engine/cards/WEA003ORF.js';

describe('ATT002OPR', () => {
    it('should not be restless on first victory, and should be jubilant', () => {
        var game = new GameEngine();
        var card = new ATT002OPR(game);
        card.victory();
        expect(card.attributes.current.restless).to.equal(false);
        expect(card.attributes.current.jubilant).to.equal(true);
    });

    it('should set the weather to Ork Fog when it has been victorious twice', () => {
        var game = new GameEngine();
        var card = new ATT002OPR(game);
        card.victory();
        card.victory();
        var side = game.active;
        var weather = side.setting.getWeather();
        expect(weather.id).to.equal('WEA003ORF');
    });

    it('should not be jubilant on first idle, and should be restless', () => {
        var game = new GameEngine();
        var card = new ATT002OPR(game);
        card.idle();
        expect(card.attributes.current.restless).to.equal(true);
        expect(card.attributes.current.jubilant).to.equal(false);
    });

    it('should attack the left most hero on the board when it has been idle twice', () => {
        var game = new GameEngine();
        var side = game.active;

        var defense = new DefensiveHero(game);
        var card = new ATT002OPR(game);
        card.attack = function(c) { this.attacked = c; }

        side.defense = defense;
        side.offense = card;

        card.idle();
        card.idle();
        expect(card.attacked).to.equal(defense);
    });

    it('should attack itself when it has been idle twice and it is the left most hero', () => {
        var game = new GameEngine();
        var side = game.active;

        var card = new ATT002OPR(game);
        card.attack = function(c) { this.attacked = c; }
        side.offense = card;
        
        card.idle();
        card.idle();
        expect(card.attacked).to.equal(card);
    });

    it('should set the weather to Ork Fog when the target of the attack dies', () => {
        var game = new GameEngine();
        var side = game.active;

        var defense = new DefensiveHero(game);

        var card = new ATT002OPR(game);
        card.attack = function(c) { c.death(); }

        side.defense = defense;
        side.offense = card;
        
        card.idle();
        card.idle();

        expect(side.setting.getWeather().id).to.equal('WEA003ORF');
        expect(defense.active).to.equal(false);
        expect(card.active).to.equal(true);
    });

    it('should revert to daylight if it dies', () => {
        var game = new GameEngine();
        var side = game.active;
        var card = new ATT002OPR(game);

        side.offense = card;
        side.weather = new WEA003ORF(game);

        card.death();
        expect(side.setting.getWeather().id).to.equal('WEA001DAY');
    });

    it('should make no change to the weather if it kills itself', () => {
        var game = new GameEngine();
        var side = game.active;
        var oldWeatherId = side.setting.getWeather().id;

        var card = new ATT002OPR(game);
        card.attack = function(c) { c.death(); }
        side.offense = card;

        card.idle();
        card.idle();

        expect(card.active).to.equal(false);
        expect(side.setting.getWeather().id).to.equal(oldWeatherId);
    });
});