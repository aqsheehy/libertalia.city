import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import { Combative } from '../../src/engine/card.js';
import ATT004THF from '../../src/engine/cards/ATT004THF.js';

describe('ATT004THF', () => {
    it('should tax 10 points from the active slot', () => {
        var game = new GameEngine();
        var card = new ATT004THF(game);
        var active = game.active;
        active.points.add(100);
        card.rest();
        expect(active.points.total).to.equal(90);
        expect(card.attributes.current.bounty).to.equal(10);
    });

    it('should not tax if there are no points to take', () => {
        var game = new GameEngine();
        var card = new ATT004THF(game);
        var active = game.active;
        card.rest();
        expect(active.points.total).to.equal(0);
        expect(card.attributes.current.bounty).to.equal(0);
    });

    it('should only tax the available points', () => {
        var game = new GameEngine();
        var card = new ATT004THF(game);
        var active = game.active;
        active.points.add(8);
        card.rest();
        expect(active.points.total).to.equal(0);
        expect(card.attributes.current.bounty).to.equal(8);
    });
});