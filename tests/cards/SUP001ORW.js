import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import SUP001ORW, { ATTRIBUTES } from '../../src/engine/cards/SUP001ORW.js';
import { DefensiveHero, CombativeDecorator } from '../../src/engine/card.js'; 

describe('SUP001ORW', () => {

    it('should be able to play onto the board', () => {
        var game = new GameEngine();
        var sup001orw = new SUP001ORW(game);
        sup001orw.play();
        var side = game.active;
        expect(side.heroes.getSupport()).to.equal(sup001orw);
    });
        
    it('should grant +1/+1 to all orcs on the board on victory', () => {
        var game = new GameEngine();
        var side = game.active;
        
        var sup001orw = new SUP001ORW(game);
        
        var otherOrk = new OrkHero(game);
        var notOrk = new DefensiveHero(game);
        
        side.defense = notOrk;
        side.offense = otherOrk;
        side.support = sup001orw;        
        
        sup001orw.victory();
        
        expect(sup001orw.attributes.current.damage).to.equal(ATTRIBUTES.damage + 1);
        expect(sup001orw.attributes.current.health).to.equal(ATTRIBUTES.health + 1);
            
        expect(otherOrk.attributes.current.damage).to.equal(1);
        expect(otherOrk.attributes.current.health).to.equal(1);
            
        expect(notOrk.attributes.current.damage).to.equal(undefined);
        expect(notOrk.attributes.current.health).to.equal(undefined);
    });
    
    it('should make the biggest ork attack the enemy impediment on victory if there is one', () => {
        var game = new GameEngine();
        var side = game.active;
        var enemySide = game.inactive;
        
        var sup001orw = new SUP001ORW(game);
        
        var bigOrk = new OrkHero(game);
        bigOrk.attributes.set({ damage: 10, health: 1 });
        
        var biggerOrk = new OrkHero(game);
        biggerOrk.attributes.set({ damage: 11, health: 1 });
        biggerOrk.attack = function(){ this.attacked = true; };
        
        var impediment = new DefensiveHero(game);
        impediment.attack = function(){ this.attacked = true }; 
        
        side.defense = biggerOrk;
        side.offense = bigOrk;
        side.support = sup001orw;
        enemySide.impediment = impediment;
        
        sup001orw.victory();
        expect(biggerOrk.attacked).to.equal(true);
        expect(impediment.attacked).to.equal(true);
    });
    
    it('should make the biggest ork move to the impediment slot if there is no impediment', () => {
        var game = new GameEngine();
        var side = game.active;
        var enemySide = game.inactive;
        
        var sup001orw = new SUP001ORW(game);
        
        var bigOrk = new OrkHero(game);
        bigOrk.attributes.set({ damage: 10, health: 1 });
        
        var biggerOrk = new OrkHero(game);
        biggerOrk.attributes.set({ damage: 11, health: 1 });
        biggerOrk.attack = function(){ this.attacked = true; }; 

        side.defense = biggerOrk;
        side.offense = bigOrk;
        side.support = sup001orw;
        
        sup001orw.victory();
        
        var impediment = enemySide.setting.getImpediment();
        expect(impediment).to.equal(biggerOrk);
        var defense = side.heroes.getDefense();
        expect(defense).to.equal(null);
    });
    
    it('should attack the strongest other hero on its side on defeat', () => {
        var game = new GameEngine();
        var side = game.active;
        var enemySide = game.inactive;
        
        var sup001orw = new SUP001ORW(game);
        sup001orw.attack = function(){};
        
        var bigOrk = new OrkHero(game);
        bigOrk.attributes.set({ damage: 2, health: 1 });
        bigOrk.attack = function(){ this.attacked = true; };
        
        side.offense = bigOrk;
        side.support = sup001orw;

        sup001orw.defeat();
        expect(bigOrk.attacked).to.equal(undefined);
        
        bigOrk.attributes.set({ damage: 5, health: 1 });
        sup001orw.defeat();
        expect(bigOrk.attacked).to.equal(true);
    });
    
});

class OrkHero extends DefensiveHero {
    get types(){ return ['Ork'].concat(super.types); }
}