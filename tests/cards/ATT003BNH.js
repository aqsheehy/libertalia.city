import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import { Combative } from '../../src/engine/card.js';
import ATT003BNH from '../../src/engine/cards/ATT003BNH.js';
import StandardDefend from '../../src/engine/cards/core/defend.js'

describe('ATT003BNH', () => {
    it('should not attack cards without a bounty on them', () => {
        var game = new GameEngine();
        var card = new ATT003BNH(game);
        var target = new Combative(game);
        target.attributes.set({ damage: 1, health: 10 });
        target.defend = function() { this.defended = true; };
        card.attack(target);
        expect(target.defended).to.equal(undefined);
    });

    it('should attack card with bounties, and double them', () => {
        var game = new GameEngine();
        var card = new ATT003BNH(game);
        var target = new Combative(game);
        target.attributes.set({ damage: 1, health: 10, bounty: 100 });
        target.defend = function() { this.defended = true; };
        card.attack(target);
        expect(target.attributes.current.bounty).to.equal(200);
        expect(target.defended).to.equal(true);
    });
});