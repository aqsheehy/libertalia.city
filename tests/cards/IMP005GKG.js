import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import IMP005GKG from '../../src/engine/cards/IMP005GKG.js';

describe('IMP005GKG', () => {
    it('should increase its increment and then tax both sides on rest', () => {
        var game = new GameEngine();
        var card = new IMP005GKG(game);
        card.taxed = [];
        card.tax = function(side) { this.taxed.push(side); };
        card.rest();
        expect(card.attributes.current.taxIncrement).to.equal(5);
        expect(card.taxed.length).to.equal(2);
        expect(card.taxed[0]).to.equal(game.active);
        expect(card.taxed[1]).to.equal(game.inactive);
    });

    it('should steal the current increment points on successful tax', () => {
        var game = new GameEngine();
        var side = game.active;
        side.points.add(10);
        var card = new IMP005GKG(game);
        card.attributes.add('taxIncrement', 5);
        card.tax(side);
        expect(side.points.total).to.equal(5);
        expect(card.attributes.current.bounty).to.equal(5);
    });

    it('should steal the current increment points on unsuccessful tax, only adding the delta to the bounty', () => {
        var game = new GameEngine();
        var side = game.active;
        side.points.add(4);
        var card = new IMP005GKG(game);
        card.attributes.add('taxIncrement', 5);
        card.tax(side);
        expect(side.points.total).to.equal(-1);
        expect(card.attributes.current.bounty).to.equal(4);
    });
});
