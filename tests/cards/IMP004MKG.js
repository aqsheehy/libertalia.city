import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import { OffensiveHero } from '../../src/engine/card.js';
import IMP004MKG from '../../src/engine/cards/IMP004MKG.js';

describe('IMP004MKG', () => {
    it('should tax both sides on rest', () => {
        var game = new GameEngine();
        var card = new IMP004MKG(game);
        card.taxed = [];
        card.tax = function(side) { this.taxed.push(side); };
        card.rest();
        expect(card.taxed.length).to.equal(2);
        expect(card.taxed[0]).to.equal(game.active);
        expect(card.taxed[1]).to.equal(game.inactive);
    });

    it('should steal 10 points on successful tax', () => {
        var game = new GameEngine();
        var side = game.active;
        side.points.add(10);
        var card = new IMP004MKG(game);
        card.tax(side);
        expect(side.points.total).to.equal(0);
        expect(card.attributes.current.bounty).to.equal(10);
    });

    it('should steal 10 points on unsuccessful tax, and kill the left most hero, only adding the delta to the bounty', () => {
        var game = new GameEngine();
        var side = game.active;
        side.points.add(8);
        var leftMost = new OffensiveHero(game);
        side.offense = leftMost
        var card = new IMP004MKG(game);
        card.tax(side);
        expect(side.points.total).to.equal(-2);
        expect(card.attributes.current.bounty).to.equal(8);
        expect(leftMost.active).to.equal(false);
    });
});
