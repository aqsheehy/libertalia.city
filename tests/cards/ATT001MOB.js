import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import { Impediment, Environment, Spell } from '../../src/engine/card.js';
import ATT001MOB from '../../src/engine/cards/ATT001MOB.js';

class TestSpell extends Spell {
    get types(){ return ['Blood'].concat(super.types); }
    play() { this.deploy(); }
    deploy(){ this.deployed = true; }
}

describe('ATT001MOB', () => {

    it('should be able to be played onto the board', () => {
        var game = new GameEngine();
        var card = new ATT001MOB(game);
        card.play();
        var side = game.active;
        expect(side.heroes.getOffense()).to.equal(card);
    });
        
    it('should cast the first blood spell from the current hand on kill and store it', () => {
        
        var game = new GameEngine();
        var side = game.active;
        side.environment = new Environment(game);
        
        var spell = new TestSpell(game);
        side.hand.push(spell);
        
        var card = new ATT001MOB(game);
        card.kill(new Impediment(game));
       
        expect(spell.deployed).to.equal(true);
        expect(card.castSpells.length).to.equal(1);
                
    });
    
    it('should <no longer> die if there are no blood spells in the current hand when it kills something', () => {
        var game = new GameEngine();
        var side = game.active;
        side.environment = new Environment(game);
                
        var card = new ATT001MOB(game);
        card.kill(new Impediment(game));
        
        var attr = card.attributes;
        // this used to remove any incentive to play this card.
        expect(attr.get().hasDied).to.equal(undefined);
    });

    it('should become restless on idle', () => {
        var game = new GameEngine();                
        var card = new ATT001MOB(game);
        card.idle();
        expect(card.attributes.current.restless).to.equal(true);
    });

    it('should attack itself if it become restless again', () => {
        var game = new GameEngine();                
        var card = new ATT001MOB(game);
        card.attack = function(c){ this.attacked = c; }
        card.idle();
        card.idle();
        expect(card.attacked).to.equal(card);
    });
});

