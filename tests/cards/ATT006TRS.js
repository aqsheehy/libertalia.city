import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import { Card } from '../../src/engine/card.js';
import ATT006TRS from '../../src/engine/cards/ATT006TRS.js';

describe('ATT006TRS', () => {
    it('should tax, trade and then travel on rest', () => {
        var game = new GameEngine();
        var card = new ATT006TRS(game);
        card.tax = function(){ this.taxed = true; }
        card.trade = function(){ this.traded = true; }
        card.travel = function(){ this.travelled = true; }
        card.rest();
        expect(card.taxed).to.equal(true);
        expect(card.traded).to.equal(true);
        expect(card.travelled).to.equal(true);
    });

    it('should tax 5 points and add to its bounty', () => {
        var game = new GameEngine();
        var card = new ATT006TRS(game);
        card.tax();
        expect(game.active.points.total).to.equal(-5);
        expect(card.attributes.current.bounty).to.equal(5);
    });

    it('should cache the left most card from the active hand', () => {
        var game = new GameEngine();
        var card = new ATT006TRS(game);
        var toTake = new Card(game);
        game.active.hand.push(toTake);
        
        card.trade();
        expect(card.cachedCard).to.equal(toTake);
    });

    it('should trade the left most card from the active hand with the cached card', () => {
        var game = new GameEngine();
        var card = new ATT006TRS(game);
        var toTake = new Card(game);
        game.active.hand.push(toTake);
        
        card.trade();
        expect(card.cachedCard).to.equal(toTake);

        var toTrade = new Card(game);
        game.active.hand.push(toTrade);

        card.trade();
        expect(card.cachedCard).to.equal(toTrade);
        expect(game.active.hand[0]).to.equal(toTake);
    });

    it('should first travel to the defense if there is none', () => {
        var game = new GameEngine();
        var card = new ATT006TRS(game);
        card.travel();
        expect(game.inactive.defense).to.equal(card);
    });

    it('should first travel to the offense if there is a defense', () => {
        var game = new GameEngine();
        var card = new ATT006TRS(game);
        game.inactive.defense = new Card(game);
        card.travel();
        expect(game.inactive.offense).to.equal(card);
    });

    it('should first travel to the support if there is both a defense and offense', () => {
        var game = new GameEngine();
        var card = new ATT006TRS(game);
        game.inactive.defense = new Card(game);
        game.inactive.offense = new Card(game);
        card.travel();
        expect(game.inactive.support).to.equal(card);
    });

});