import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import IMP009BLT from '../../src/engine/cards/IMP009BLT.js';
import { StandardDefensiveHero, StandardImpediment, StandardSupportiveHero, StandardOffensiveHero } from '../../src/engine/cards/core/card.js';

describe('IMP009BLT', () => {

    it('should create a black tar if the target died on attack and buff all tars by +1/+1', () => {
        var game = new GameEngine();
        game.active.impediment = new IMP009BLT(game);
        game.active.defense = new StandardDefensiveHero(game, { damage: 1, health: 1 });
        game.active.impediment.attack(game.active.defense);
        expect(game.active.defense.id).to.equal('IMP009BLT');
    });

    it('should be able to replace offensive heroes', () => {
        var game = new GameEngine();
        game.active.impediment = new IMP009BLT(game);
        game.active.offense = new StandardOffensiveHero(game, { damage: 1, health: 1 });
        game.active.impediment.attack(game.active.offense);
        expect(game.active.offense.id).to.equal('IMP009BLT');
    });

    it('should be able to replace supportive heroes', () => {
        var game = new GameEngine();
        game.active.impediment = new IMP009BLT(game);
        game.active.support = new StandardSupportiveHero(game, { damage: 1, health: 1 });
        game.active.impediment.attack(game.active.support);
        expect(game.active.support.id).to.equal('IMP009BLT');
    });

    it('should be able to replace impediments', () => {
        var game = new GameEngine();
        game.active.defense = new IMP009BLT(game);
        game.active.impediment = new StandardImpediment(game, { damage: 1, health: 1 });
        game.active.defense.attack(game.active.impediment);
        expect(game.active.impediment.id).to.equal('IMP009BLT');
    });

    it('should not attack other black tars', () => {
        var game = new GameEngine();
        var card = new IMP009BLT(game);
        var target = new IMP009BLT(game);
        card.attack(target);
        expect(target.active).to.equal(true);
    });

    it('should be able to locate all black tars on both sides', () => {
        var game = new GameEngine();
        game.active.defense = new IMP009BLT(game);
        game.active.offense = new IMP009BLT(game);
        game.active.support = new IMP009BLT(game);
        game.active.impediment = new IMP009BLT(game);
        game.inactive.defense = new IMP009BLT(game);
        game.inactive.offense = new IMP009BLT(game);
        game.inactive.support = new IMP009BLT(game);
        game.inactive.impediment = new IMP009BLT(game);
        expect(game.active.defense.allBlackTar().length).to.equal(8);
    });

    it('should be able to buff all tars by +1/+1', () => {
        var game = new GameEngine();
        game.active.defense = new IMP009BLT(game);
        game.active.offense = new IMP009BLT(game);
        game.active.support = new IMP009BLT(game);
        game.active.impediment = new IMP009BLT(game);
        game.inactive.defense = new IMP009BLT(game);
        game.inactive.offense = new IMP009BLT(game);
        game.inactive.support = new IMP009BLT(game);
        game.inactive.impediment = new IMP009BLT(game);
        game.active.defense.buffAllTars();
        game.active.defense.allBlackTar().forEach(c => {
            expect(c.attributes.current.damage).to.equal(2);
            expect(c.attributes.current.health).to.equal(2);
        });
        
    });

    it('should replace the killed card with tar and then buff all tars', () => {
        var game = new GameEngine();

        var card = new IMP009BLT(game);
        card.replaceWithTar = function(c) { this.replaced = true; }
        card.buffAllTars = function() { this.buffed = true; }
        game.active.impediment = card;

        var target = new StandardDefensiveHero(game, { damage: 1, health: 1 });
        game.active.defense = target;

        card.attack(target);

        expect(card.replaced).to.equal(true);
        expect(card.buffed).to.equal(true);
    });

});