import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import SUP007FRM from '../../src/engine/cards/SUP007FRM.js';

describe('SUP007FRM', () => {
    it('should add 50 points to both sides on rest', () => {
        var game = new GameEngine();
        var card = new SUP007FRM(game);
        card.rest();
        expect(game.active.points.total).to.equal(50);
        expect(game.inactive.points.total).to.equal(50);
        card.rest();
        expect(game.active.points.total).to.equal(100);
        expect(game.inactive.points.total).to.equal(100);
    });
});