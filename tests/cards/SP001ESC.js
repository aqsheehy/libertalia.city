import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import { Impediment, Environment } from '../../src/engine/card.js';
import SP001ESC from '../../src/engine/cards/SP001ESC.js';

describe('SP001ESC', () => {
        
    it('should change the environment on deploy to the tavern', () => {
        
        var game = new GameEngine();
        var side = game.active;
        side.environment = new Environment(game);
        
        var card = new SP001ESC(game);
        card.play();
        
        expect(side.setting.getEnvironment().id).to.equal('ENV001TAV');
        
    });
    
    it('should kill the impediment', () => {
        
        var game = new GameEngine();
        game.active.impediment = new Impediment(game);
        
        var card = new SP001ESC(game);
        card.play();
        
        expect(game.active.impediment.active).to.equal(false);
        
    });
    
});