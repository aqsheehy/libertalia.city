import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import IMP006TMR from '../../src/engine/cards/IMP006TMR.js';
import { Card, CombativeDecorator } from '../../src/engine/card.js';

describe('IMP006TMR', () => {
    it('should not play onto the board if there is already an impediment', () => {
        var game = new GameEngine();
        var side = game.active;

        var a = new IMP006TMR(game);
        side.impediment = a;

        var b = new IMP006TMR(game);
        b.play();

        expect(side.impediment).to.equal(a);
    });

    it('should normally become the impediment on play, and set the environment to ENV003MRP', () => {
        var game = new GameEngine();
        var side = game.active;
        var card = new IMP006TMR(game);
        card.play();
        expect(side.impediment).to.equal(card);
        expect(side.environment.id).to.equal('ENV003MRP');
    });

    it('should set the health of all non-mirror heroes on the active side to 1', () => {
        var game = new GameEngine();
        var side = game.active;

        var offense = new Card(game);
        offense.attributes.modify('health', 10);
        side.offense = offense;

        var defense = new Card(game);
        defense.attributes.modify('health', 10);
        side.defense = defense;

        var support = new MirrorDecorator(new Card(game));
        support.attributes.modify('health', 10);
        side.support = support;

        var card = new IMP006TMR(game);
        card.play();

        expect(offense.attributes.current.health).to.equal(1);
        expect(defense.attributes.current.health).to.equal(1);
        expect(support.attributes.current.health).to.equal(10);
    });
});

class MirrorDecorator extends CombativeDecorator {
    get types(){ return ['Mirror'].concat(super.types); }
}
