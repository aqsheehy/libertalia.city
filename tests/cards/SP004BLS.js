import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import { Card, CombativeDecorator, DefensiveHero } from '../../src/engine/card.js';
import SP004BLS from '../../src/engine/cards/SP004BLS.js';

describe('SP004BLS', () => {

    it('should do nothing if there is no blood mage on this side', () => {
        var game = new GameEngine();
        var enemySide = game.inactive;
        var enemyDefense = new DefensiveHero(game);
        enemySide.defense = enemyDefense;
        var spell = new SP004BLS(game);
        spell.play();

        expect(enemySide.heroes.getDefense()).to.equal(enemyDefense);
    });

    it('should kill the left-most enemy hero if there is a blood mage on this side', () => {
        var game = new GameEngine();

        var thisSide = game.active;
        var bloodMage = new TypeDecorator(new DefensiveHero(game), ['Blood', 'Mage']);
        thisSide.offense = bloodMage;

        var enemySide = game.inactive;
        var enemyDefense = new DefensiveHero(game);
        enemySide.defense = enemyDefense;
        var spell = new SP004BLS(game);

        spell.play();

        expect(enemySide.heroes.getDefense().active).to.equal(false);
    });

    it('should kill the left-most hero if there is a blood mage on the enemy side', () => { 
        var game = new GameEngine();

        var thisSide = game.active;
        var thisDefense = new DefensiveHero(game);
        thisSide.offense = thisDefense;
        
        var enemySide = game.inactive;
        var bloodMage = new TypeDecorator(new DefensiveHero(game), ['Blood', 'Mage']);
        enemySide.defense = bloodMage;
        var spell = new SP004BLS(game);

        spell.play();

        expect(thisSide.heroes.getDefense()).to.equal(null);
    });

    it('should kill both of the left-most heroes if there are blood mages on both sides', () => {
        var game = new GameEngine();

        var bloodMage1 = new TypeDecorator(new DefensiveHero(game), ['Blood', 'Mage']);

        var thisSide = game.active;
        var thisDefense = new DefensiveHero(game);
        thisSide.defense = thisDefense;
        thisSide.offense = bloodMage1;
        
        var bloodMage2 = new TypeDecorator(new DefensiveHero(game), ['Blood', 'Mage']);

        var enemySide = game.inactive;
        var enemyDefense = new DefensiveHero(game);
        enemySide.defense = enemyDefense;
        enemySide.offense = bloodMage2;
        var spell = new SP004BLS(game);

        spell.play();

        expect(thisSide.heroes.getDefense().active).to.equal(false);
        expect(enemySide.heroes.getDefense().active).to.equal(false);
    });

});

class TypeDecorator extends CombativeDecorator {
    constructor(card, types){
        super(card);
        this.$types = types;
    }

    get types(){ return this.$types.concat(super.types); }
}