import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import { Card } from '../../src/engine/card.js';
import IMP002SPP from '../../src/engine/cards/IMP002SPP.js';

describe('IMP002SPP', () => {
    it('should buff any target it attacks by +1 health', () => {
        var game = new GameEngine();
        var card = new IMP002SPP(game);
        var target = new Card(game);
        target.attributes.set({ damage: 1, health: 1 });
        card.attack(target);
        expect(target.attributes.current.health).to.equal(2);
    });

    it('should buff any target it is attacked by, by +1 damage', () => {
        var game = new GameEngine();
        var card = new IMP002SPP(game);
        var target = new Card(game);
        target.attributes.set({ damage: 1, health: 1 });
        card.defend(target);
        expect(target.attributes.current.damage).to.equal(2);
    });

    it('dies if it defends itself and has health between 1 and 3', () => {
        var game = new GameEngine();
        var card = new IMP002SPP(game);
        card.attributes.set({ damage: 0, health: 1 });
        var target = new Card(game);
        target.attributes.set({ damage: 1, health: 1 });
        card.defend(target);
        expect(target.attributes.current.damage).to.equal(2);
        expect(card.attributes.current.hasDied).to.equal(true);
    });

    it('will die directly if it has less than 0 health', () => {
        var game = new GameEngine();
        var card = new IMP002SPP(game);
        card.attributes.set({ damage: 0, health: 0 });
        card.death = function() { this.directDeath = true; };
        var target = new Card(game);
        target.attributes.set({ damage: 1, health: 1 });
        card.defend(target);
        expect(target.attributes.current.damage).to.equal(2);
        expect(card.directDeath).to.equal(true);
    });

    it('will subtract 100 points from the active side if it dies directly', () => {
        var game = new GameEngine();
        var card = new IMP002SPP(game);
        game.active.impediment = card;
        card.death();

        expect(game.active.points.total).to.equal(-100);
    });
});