import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import { Card, DefensiveHero } from '../../src/engine/card.js';
import DEF003OLS from '../../src/engine/cards/DEF003OLS.js';

describe('DEF003OLS', () => {
    it('should tax the active side if there are no bounty heroes or impediments', () => {
        var game = new GameEngine();
        var card = new DEF003OLS(game);
        card.idle();
        var active = game.active;
        expect(active.points.total).to.equal(-50);
    });

    it('should attack the first inactive side bounty hero first', () => {
        var game = new GameEngine();
        var card = new DEF003OLS(game);
        card.attack = function(c) { this.attacked = c };
        var active = game.active;
        active.heroes[0] = new DefensiveHero(game);
        active.heroes[0].attributes.current.bounty = 1;

        card.idle();
        expect(active.points.total).to.equal(0);
        expect(card.attacked).to.equal(active.heroes[0]);
    });
});