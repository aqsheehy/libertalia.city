import chai, { expect } from 'chai';
import GameEngine from '../../src/engine/game.js';
import SP002UPD from '../../src/engine/cards/SP002UPD.js';
import DEF001BOR from '../../src/engine/cards/DEF001BOR.js';

describe('SP002UPD', () => {
        
    it('should flip the sides around when played', () => {
        
        var game = new GameEngine();
        var activeSide = game.active;
        var inactiveSide = game.inactive;
        var card = new SP002UPD(game);
        card.play();

        expect(game.active).to.equal(inactiveSide);
        expect(game.inactive).to.equal(activeSide);
    });
    
});