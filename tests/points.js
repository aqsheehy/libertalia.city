import chai, { expect } from 'chai';
import Points from '../src/engine/points.js';

describe('Points', () => {
    it('should initialize to 0 points', () => {
        var points = new Points();
        expect(points.total).to.equal(0);
    });

    it('should be able to add values to its total', () => {
        var points = new Points();
        points.add(2);
        expect(points.total).to.equal(2);
    });

    it('should be able to subtract values from its total', () => {
        var points = new Points();
        points.subtract(2);
        expect(points.total).to.equal(-2);
    });
});