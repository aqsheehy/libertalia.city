import GameEngine from './src/engine/game.js';
import CardCatalog from './src/engine/catalog.js';
import GameLoader from './src/engine/loader.js';
import { TextPlayer } from './src/engine/player.js';
import fs from 'fs';
import TurnManager from './src/engine/turns.js';

const DEFAULT_LOCATION = './tests/game/test.json';
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

console.log(`# Welcome to the libertalia game shell...`);

rl.question('# Where is your game file located? (OPTIONAL) ', async (location) => {

    location = location || DEFAULT_LOCATION;
    var config = JSON.parse(fs.readFileSync(location, 'utf8'));
    var game = GameLoader.loadTestDeckAndRandomize(config);

    var player1 = new TextPlayer('Player1', game, (q, fn) => rl.question(q, fn), (t) => console.log(t));
    var player2 = new TextPlayer('Player2', game, (q, fn) => rl.question(q, fn), (t) => console.log(t));

    var turns = new TurnManager(game, player1, player2);
    await turns.run();
    
});
