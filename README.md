# Libertalia.city

To initialize the project, first...

npm install

npm install -g gulp

npm install -g mocha

## Running the client 

npm start

## Executing the unit tests 

npm test

## Running the cmd

npm run cmd

## Trello

https://trello.com/b/ZrNAovWt

## Also if you're using VS Code 

ctrl+shift+b => Build the app into the running web app again