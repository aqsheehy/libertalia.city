class Points {
    constructor(){
        this.total = 0;
    }

    add(i){
        this.total += (i || 1);
    }

    subtract(i){
        this.total -= (i || 1);
    }

}

export default Points;