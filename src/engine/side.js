import { CardCollection, HeroesCollection, SettingCollection, Hand, Deck, Graveyard } from './cards.js';
import Points from './points.js';


class PlayerSide {
      
    constructor(key, game) {
        this.player = key;
        this.game = game;

        this.deck = new Deck(key);
        this.hand = new Hand(key);
        this.grave = new Graveyard(key);
        this.heroes = new HeroesCollection(key);
        this.setting = new SettingCollection(key);

        this.points = new Points();
    }

    get impediment(){ return this.setting.getImpediment(); }
    set impediment(val){ return this.setting.setImpediment(val); }

    get weather(){ return this.setting.getWeather(); }
    set weather(val){ return this.setting.setWeather(val); }

    get environment(){ return this.setting.getEnvironment(); }
    set environment(val){ return this.setting.setEnvironment(val); }

    get defense(){ return this.heroes.getDefense(); }
    set defense(val){ return this.heroes.setDefense(val); }

    get offense(){ return this.heroes.getOffense(); }
    set offense(val){ return this.heroes.setOffense(val); }

    get support(){ return this.heroes.getSupport(); }
    set support(val){ return this.heroes.setSupport(val); }
}

export default PlayerSide;