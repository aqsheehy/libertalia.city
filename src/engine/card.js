import AttributeStore from './attributes.js';
import Location from './location.js';

class Card {
    
    constructor(game) { 
        this.$attributes = new AttributeStore(); 
        this.$game = game;
        this.$location = new Location(this);
    }
        
    get types() { return ['Card']; }
    get name() { return; }
    get description() { return; }
    get id() { return; }
    get active() { return !this.$attributes.get().hasDied; }
    get attributes() { return this.$attributes; }
    get game() { return this.$game; }
    get location() { return this.$location; } 

    // Play this card onto the board
    play() { this.game.events.publish('play', this); }

    // Triggered after a card is moved into a slot
    deploy() { this.game.events.publish('deploy', this); }

    // Depends on the combat logic, typically if there was nothing to do
    idle() { this.game.events.publish('idle', this); }

    // Depends on the combat logic, typically triggered on alive heroes after combat ends
    rest() { this.game.events.publish('rest', this); }

    // Called when this is drawn
    draw() { this.game.events.publish('draw', this); }

    // Called when this is discarded
    discard() { this.game.events.publish('discard', this); }

    // Called when this has died
    death() { 
        this.attributes.modify('hasDied', true); 
        this.game.events.publish('death', this);
    }
}

class Combative extends Card {

    get types() { return ['Combative'].concat(super.types); }
    
    // Called when this attacks a given card
    attack(card) { this.game.events.publish('attack', this); }

    // Called when a given card attacks this
    defend(card) { this.game.events.publish('defend', this); }

    // Depends on the combat logic, typically triggered when all enemies die
    victory() { this.game.events.publish('victory', this); }

    // Depends on the combat logic, typically triggered when all friendlies die
    defeat() { this.game.events.publish('defeat', this); }
}

class CardDecorator {

    constructor(card) { 
        this.source = card; 
        this.location.card = this;
    }
    
    get types() { return this.source.types; }
    get name() { return this.source.name; }
    get description() { return this.source.description; }
    get id() { return this.source.id; }
    get active() { return this.source.active; }
    get attributes() { return this.source.attributes; }
    get game() { return this.source.game; } 
    get location(){ return this.source.location; }
    
    play() { return this.source.play(); }
    deploy() { return this.source.deploy(); }
    death() { return this.source.death(); }
    idle() { return this.source.idle(); }

    rest() { this.source.rest(); }
    draw() { this.source.draw(); }
    discard() { this.source.discard(); }
}

class CombativeDecorator extends CardDecorator {    
    attack(card) { this.source.attack(card); }
    defend(card) { this.source.defend(card); }
    kill(card) { this.source.kill(card); }
    victory() { this.source.victory(); }
    defeat() { this.source.defeat(); }
    travel() { this.source.travel(); }
    weather() { this.source.weather(); }
}

class Hero extends Combative {
    get types() { return ['Hero'].concat(super.types); }
}

class DefensiveHero extends Hero {
    get types() { return ['Defensive'].concat(super.types); }
}

class OffensiveHero extends Hero {
    get types() { return ['Offensive'].concat(super.types); }
}

class SupportiveHero extends Hero {
    get types() { return ['Supportive'].concat(super.types); }
}

class Weather extends Card {
    get types() { return ['Weather'].concat(super.types); }
    death() { /* Default behaviour should be to do nothing */ }
    getCombat() { throw new Error('Each weather should have a combat logic associated with it.') }
    play(){
        this.game.events.publish('weather', this);
        super.play();
    }
}

class Impediment extends Combative {
    get types() { return ['Impediment'].concat(super.types); }
}

class Environment extends Card {
    get types() { return ['Environment'].concat(super.types); }
    death() { /* Default behaviour should be to do nothing */ }
    getCombat(weather) { return weather.getCombat(); }
}

class Spell extends Card {
    get types() { return ['Spell'].concat(super.types); }
}

export { Card, CardDecorator, Combative, CombativeDecorator, DefensiveHero, OffensiveHero, SupportiveHero, Weather, Impediment, Environment, Spell  };