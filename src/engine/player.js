class TextPlayer {

    constructor(name, game, read, write) { 
        this.$name = name; 
        this.game = game;
        this.read = read;
        this.write = write;
    }

    get name() { return this.$name; }

    async decide(options) {
        
        var side = this.game.active;
        this.write('');
        this.printCollection('Heroes', side.heroes);
        this.printCollection('Setting', side.setting);
        this.printCollection('Hand', side.hand);
        this.write(`# Points (${side.points.total}),  Deck (${side.deck.length}), Grave (${side.grave.length})`);

        return new Promise((resolve, reject) => {
            this.read(`# You can choose to either ${options.join()}: `, (answer) => {

                var parts = answer.split(' ');

                if (options.indexOf(parts[0]) === -1)  {
                    this.write('!!!!!!!!!!!!!! Invalid input received, finishing turn. !!!!!!!!!!!!!!');                    
                    resolve({ type: 'Finish' });
                }
                else {
                    var decision = { type: parts[0] };
                    var index = parts[1];
                    if (index) decision.index = parseInt(index);
                    resolve(decision);
                }
            });
        });
    }

    printCollection(name, collection) {
        collection = collection.map(this.cardToString);
        var display = collection.length == 0 ? '<Empty>' : collection.length > 10 ? collection.slice(0, 10).join() : collection.join();
        this.write(`# ${name} (${collection.length}): ${display}`);
    }

    cardToString(c, i){
        if (!c) return `${i}:<Empty>`;
        var attr = c.attributes;
        var text = `${i}:${c.name}`;
        if (attr.current.health) {
            var damage = attr.current.damage === undefined ? '?' : attr.current.damage; 
            var health = attr.current.health === undefined ? '?' : attr.current.health;
            text += `[${damage}/${health}]`;
        }
        if (!c.active){
            text += "*D*"
        }
        return text;
    }
}

export { TextPlayer };