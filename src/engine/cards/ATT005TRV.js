import { StandardOffensiveHero } from './core/card.js';

const TRAVEL_LIMIT = 6;
const TRAVEL_BOUNTY = 200;
const TRAVEL_PENALTY = 10;
const ATTRIBUTES = { damage: 2, health: 2 }
const DESCRIPTION = `
Rest: Travels left or right empty slot
Rest: After ${TRAVEL_LIMIT} travels, ${TRAVEL_BOUNTY} points to the active side.
Rest: If it doesn't travel, subtract ${TRAVEL_PENALTY} from active side.
`;

class ATT005TRV extends StandardOffensiveHero {
    
    constructor(game) { 
        super(game, ATTRIBUTES); 
        this.travelCount = 0;
    }

    get name() { return 'Traveller' }
    get id() { return 'ATT005TRV' }
    get types() { return ['Traveller', 'Elderly', 'Human', 'Humanoid', 'Male'].concat(super.types); }  
    get description() { return DESCRIPTION; }

    play(){
        this.travelCount = 0;
        super.play();
    }

    rest(){
        var home = this.location.home;
        var opposite = this.location.opposite;

        if (this.location.index == 0) {

            if (!opposite.support) opposite.support = this;
            else if (!home.offense) home.offense = this;
            else return this.cantMove();
            
        }
        else if (this.location.index == 1) {

            if (!home.defense) home.defense = this;
            else if (!home.support) home.support = this;
            else return this.cantMove();

        }
        else if (this.location.index == 2) {

            if (!home.offense) home.offense = this;
            else if (!opposite.defense) opposite.defense = this;
            else return this.cantMove();

        }

        this.travelled();
    }

    cantMove(){
        this.game.active.points.subtract(TRAVEL_PENALTY);
        super.rest();
    }

    travelled(){
        this.travelCount++;
        if (this.travelCount % TRAVEL_LIMIT == 0) 
            this.game.active.points.add(TRAVEL_BOUNTY);
        super.rest();
    }

}

export default ATT005TRV;