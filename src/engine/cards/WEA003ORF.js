import OrkCombatLogic from './combat/ork.js';
import { StandardWeather } from './core/card.js';
import StandardBuff from './core/buff.js';

const REST_BUFF = "+1/+1";
const RestBuff = StandardBuff.fromString(REST_BUFF);

/* 
    Combat logic: OrkCombatLogic
    Rest: +1/+1 to all Orks
*/
class WEA003ORF extends StandardWeather {

    get name() { return 'Ork Fog' }
    get id() { return 'WEA003ORF' }
    get types() { return ['Dark', 'Fog', 'Wet'].concat(super.types); }

    rest() {
        this.game.active.heroes.ofType('Ork').forEach(RestBuff);
        super.rest();
    }

    getCombat() {
        return new OrkCombatLogic();
    }

}

export default WEA003ORF;