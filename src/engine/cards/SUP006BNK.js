import { StandardSupportiveHero } from './core/card.js';
import StandardRestless from './core/restless.js';

const INTEREST_ITERATIONS = 3;
const ATTRIBUTES = { damage: 1, health: 2, restless: false };
const DESCRIPTION = `
Play: Transfer half the active side points to the inactive side (round up) 
Rest: For ${INTEREST_ITERATIONS} iterations, transfer 1/${INTEREST_ITERATIONS} of the total initial transfer amount x2 back.
Restless: Initiate the loan again.
`;

class SUP006BNK extends StandardSupportiveHero {

    constructor(game) {
        super(game, ATTRIBUTES);
        this.iterations = 0;
        this.interest = 0;
        this.loanInitiated = false;
    }

    get name() { return 'Dwarven Banker'; }
    get id() { return 'SUP006BNK'; }
    get types() { return [ 'Banker', 'Hated', 'Male', 'Dwarf', 'Humanoid' ].concat(super.types); }
    get description() { return DESCRIPTION; }

    play() {
        this.loan();
        super.play();
    }

    rest(){
        if (this.loanInitiated && this.iterations < INTEREST_ITERATIONS){
            this.iterations++;
            this.applyInterest();
        }
        super.rest();
    }

    victory(){
        StandardRestless.victory(this);
        super.victory();
    }

    idle() {
        StandardRestless.idle(this, () => this.loan());
        super.idle();
    }

    applyInterest() {
        this.game.active.points.add(this.interest);
        this.game.inactive.points.subtract(this.interest);
    }

    loan() {
        this.loanInitiated = true;
        if (this.game.active.points.total <= 0) return;
        var totalPoints = Math.abs(this.game.active.points.total / 2);
        
        this.iterations = 0;

        this.game.inactive.points.add(totalPoints);
        this.game.active.points.subtract(totalPoints);

        this.interest = Math.ceil((totalPoints * 2) / INTEREST_ITERATIONS);
    }
}

export default SUP006BNK;