import { StandardImpediment } from './core/card.js';
import StandardBuff from './core/buff.js';
import StandardBounty from './core/bounty.js';
import StandardRestless from './core/restless.js';
import StandardJubilant from './core/jubilant.js';

const TAX_INCREMENT = 30;
const TAX_DISTRIBUTION = TAX_INCREMENT / 3;
const ATTRIBUTES = { damage: 0, health: 6, bounty: 0 };
const DESCRIPTION = `
Rest: Steal ${TAX_INCREMENT} points from both sides
Rest: Add ${TAX_DISTRIBUTION} to the bounty of all active side heroes from left to right
Rest: If there weren't enough funds taxed, any heroes who didn't benefit become restless
Rest: If there were too many funds taxed, add to the bounty of the queen.
Death: Add bounty to the active side points.
On Restless: Kill any restless heroes
`

class IMP008MQN extends StandardImpediment {
    constructor(game){ super(game, ATTRIBUTES); }

    get name() { return 'Mad Queen' }
    get id() { return 'IMP008MQN' }
    get types() { return ['Mad', 'Human', 'Female', 'Leader', 'Queen', 'Humanoid'].concat(super.types); }
    get description(){ return DESCRIPTION; }

    death(){
        StandardBounty.death(this, this.game);
        super.death();
    }

    rest() {

        var activeTax = this.tax(this.game.active);
        this.distributeTax(inactive, activeTax);

        var inactiveTax = this.tax(this.game.inactive);
        this.distributeTax(inactive, inactiveTax);

        super.rest();
    } 

    tax(side) 
    {
        var taxedAmount = side.points.total < TAX_INCREMENT ? side.points.total : TAX_INCREMENT;
        side.points.subtract(TAX_INCREMENT);
        return taxedAmount;
    }

    distributeTax(side, tax) {

        side.heroes.forEach((card, i) => {
            if (!card || !card.active) return;
            card = StandardBounty.decorate(card, this.game);
            side.heroes[i] = card;

            if (tax <= 0)
                StandardRestless.idle(card, () => {
                    if (card.active) card.death();
                });
            else
            {
                StandardBounty.add(card, tax < TAX_DISTRIBUTION ? tax : TAX_DISTRIBUTION);
                tax -= TAX_DISTRIBUTION;
            }
        });

        if (tax > 0){
            StandardBounty.add(this, tax < TAX_DISTRIBUTION ? tax : TAX_DISTRIBUTION);
        }
    }
}

export { ATTRIBUTES, TAX_INCREMENT, TAX_DISTRIBUTION }
export default IMP008MQN;