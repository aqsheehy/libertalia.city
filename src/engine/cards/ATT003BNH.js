import { StandardOffensiveHero } from './core/card.js';
import StandardBounty from './core/bounty.js';

const ATTRIBUTES = { damage: 8, health: 4 }
const DESCRIPTION = `
Attack: If it has a bounty, double it. If it doesn't, don't attack.
`;

class ATT003BNH extends StandardOffensiveHero {

    constructor(game) { super(game, ATTRIBUTES); }
    
    get name() { return 'Bounty Hunter' }
    get id() { return 'ATT003BNH' }
    get types() { return ['Human', 'Humanoid', 'Male'].concat(super.types); }  
    get description() { return DESCRIPTION; }

    attack(card) {
        var bounty = StandardBounty.get(card);
        
        if (bounty > 0) 
        {
            StandardBounty.add(card, bounty);
            super.attack(card);
        }
    }    
}

export default ATT003BNH;