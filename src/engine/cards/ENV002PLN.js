import { StandardEnvironment } from './core/card.js';

class ENV002PLN extends StandardEnvironment {
    
    get name() { return 'The Plains' }
    get id() { return 'ENV002PLN' }
    get types() { return ['Grassy', 'Flat', 'Outside', 'Moderate'].concat(super.types); }
    get description(){ return 'Sets combat logic to standard.'; }
    
    getCombat(weather) {
        return weather.getCombat();
    }
}

export default ENV002PLN;