import { StandardOffensiveHero } from './core/card.js';
import StandardBounty from './core/bounty.js';

const TRAVEL_TAX = 5;
const ATTRIBUTES = { damage: 2, health: 2, bounty: 0 }
const DESCRIPTION = `
Rest: Steals a card from the active hand and caches it.
Rest: Steal ${TRAVEL_TAX} from the active hand.
Rest: If it has a card cached, puts that card in the active hand.
Rest: Travels to the opposite board if there is any hero slot free.
Death: <Points stolen> to the active player
`;

class ATT006TRS extends StandardOffensiveHero {

    constructor(game){ super(game, ATTRIBUTES); }

    get name() { return 'Travelling Salesman' }
    get id() { return 'ATT006TRS' }
    get types() { return ['Traveller', 'Elderly', 'Human', 'Humanoid', 'Male'].concat(super.types); }  
    get description() { return DESCRIPTION; }

    rest() {
        this.tax();
        this.trade();
        this.travel();
        super.rest();
    }

    tax() {
        this.game.active.points.subtract(TRAVEL_TAX);
        StandardBounty.add(this, TRAVEL_TAX);
    }

    trade() {
        var active = this.game.active;
        if (this.cachedCard) active.hand.push(this.cachedCard);
        if (this.cachedCard = active.hand[0]) active.hand.removeAt(0);
    }

    travel() {
        var inactive = this.game.inactive;
        if (!inactive.defense) inactive.defense = this;
        else if (!inactive.offense) inactive.offense = this;
        else if (!inactive.support) inactive.support = this;
    }

    death() {
        StandardBounty.death(this, this.game);
        super.death();
    }

}

export default ATT006TRS;