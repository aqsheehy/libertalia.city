class RestingCombatLogic {
    
    execute(side) {
        var isActive = this.isActive.bind(this);

        side.heroes.filter(isActive).forEach((c) => {
            c.rest();
        });
        side.setting.filter(isActive).forEach((c) => {
            c.rest();
        });
    }

    isActive(card){
        return card && card.active;
    }
}

export default RestingCombatLogic;