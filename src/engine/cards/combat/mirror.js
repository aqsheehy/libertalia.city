/* 
1. Combat logic. 
    ForEach Hero Slot
        - Slot attack OppositeSlot
        - If OppositeSlot !dead, OppositeSlot attack Mirror
*/
class MirrorCombatLogic {
    execute(active, inactive) {

        var mirror = active.setting.getImpediment();
        if (!mirror || !mirror.active) return;

        active.heroes.forEach((hero, i) => {

            if (!mirror.active) return;
            if (!hero || !hero.active) return;

            var opposite = inactive.heroes[i];
            if (!opposite || !opposite.active) return;

            hero.attack(opposite);
            if (!opposite.active) return;

            opposite.attack(mirror);

        });
    }
}

export default MirrorCombatLogic;