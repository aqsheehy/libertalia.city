/* 
    1. Orks attack non-orks, if there are no non-orks, then they attack orks (left most always)
    2. Non-Orks idle
    3. All deaths +10 points
*/
const KILL_BONUS = 10;

class OrkCombatLogic {

    execute(side) {
        var isActive = this.isActive.bind(this);

        var impediment = side.setting.getImpediment();
        var orks = side.heroes.ofType('Ork');
        var notOrks = side.heroes.notOfType('Ork');

        if (impediment) {
            if (impediment.types.indexOf('Ork') > -1)
                orks.push(impediment);
            else
                notOrks.push(impediment);
        }

        orks.forEach((c) => {
            if (!c.active) return;
            var target = notOrks.filter(isActive)[0] || orks.filter(isActive)[0];

            if (!target)
                c.idle();
            else 
            {
                c.attack(target);
                if (!target.active)
                    side.points.add(KILL_BONUS);
            }

            c.rest();
        });

        notOrks.filter(isActive).forEach((c) => {
            c.idle();
            c.rest();
        });
    }

    isActive(card){
        return card && card.active;
    }
}

export default OrkCombatLogic;