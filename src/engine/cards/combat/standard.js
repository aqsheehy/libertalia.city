class StandardCombatLogic {
    
    execute(side) {
        var heroes = side.heroes;
        var settings = side.setting;
        var isActive = this.isActive.bind(this);
        var isNotActive = this.isNotActive.bind(this);
        var impediment = settings.getImpediment();
        var defense = heroes.getDefense();

        if (!isActive(impediment))
            heroes.filter(isActive).forEach((hero) => {
                hero.idle();
            });

        else 
        {
            if (heroes.every(isNotActive))
                impediment.idle();

            else 
            {
                heroes.filter(isActive).forEach((hero) => {
                    if (isActive(impediment)) 
                        hero.attack(impediment);

                    if (isActive(impediment) && !defense) 
                        impediment.attack(hero);
                });

                if (isActive(impediment)) {
                    if (isActive(defense)) 
                        impediment.attack(defense);

                    if (heroes.every(isNotActive))
                        impediment.victory();
                }
                else {
                    heroes.filter(isActive).forEach((hero) => {
                        hero.victory();
                    });
                }
            }
        }

        heroes.filter(isActive).forEach((card) => {
            card.rest();
        });

        settings.filter(isActive).forEach((card) => {
            card.rest();
        });
    }
    
    isActive(card){
        return card && card.active;
    }

    isNotActive(card){
        return !this.isActive(card);
    }
    
}

export default StandardCombatLogic;