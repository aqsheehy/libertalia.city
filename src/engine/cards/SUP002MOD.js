import { StandardSupportiveHero } from './core/card.js';

const DEATH_BONUS = 100;
const DEATH_CONSTRAINT = 5;
const ATTRIBUTES = { damage: 1, health: 3, restless: false, totalDeaths: 0 };
const DESCRIPTION = `
Death: ${DEATH_BONUS}+ points to active side if there have been ${DEATH_CONSTRAINT} deaths since play.
Death: ${DEATH_BONUS} - (${DEATH_BONUS / DEATH_CONSTRAINT}xDeaths) points subtracted from active side
`;

class SUP002MOD extends StandardSupportiveHero {
    constructor(game) { super(game, ATTRIBUTES); }

    get name() { return 'Martyr of Death'; }
    get id() { return 'SUP002MOD'; }
    get types() { return ['Martyr', 'Priest', 'Death'].concat(super.types); }
    get description() { return DESCRIPTION; }

    play() {
        this.game.events.subscribe('death', () => {
            if (!this.active) return;
            this.attributes.add('totalDeaths', 1);
        });
        super.play();
    }

    death() {
        
        var totalDeaths = this.attributes.current.totalDeaths;

        if (totalDeaths >= DEATH_CONSTRAINT)
            this.game.active.points.add(DEATH_BONUS);
        else 
            this.game.active.points.subtract(DEATH_BONUS - DEATH_BONUS / DEATH_CONSTRAINT * totalDeaths);

        super.death();
    }
}

export default SUP002MOD;