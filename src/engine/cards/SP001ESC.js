import { StandardSpell } from './core/card.js';
import ENV001TAV from './ENV001TAV.js';

const DESCRIPTION = `
Play: The active side impediment is victorious, then it dies.
Play: All spies on the active side are victorious, then die.
Play: All non spies on the active side are defeated.
Play: Change the environment to The Tavern.
`;

class SP001ESC extends StandardSpell {
    
    get name() { return 'Escape!' }
    get id() { return 'SP001ESC' }
    get description(){ return DESCRIPTION; }

    play() {
        var impediment = this.game.active.setting.getImpediment();
        if (impediment) {
            impediment.victory();
            impediment.death();
        }
        
        this.game.active.heroes.ofType('Spy').forEach((card) => {
            card.victory();
            card.death();
        });
        
        this.game.active.heroes.notOfType('Spy').forEach((card) => {
            card.defeat();
        })
        
        var environment = this.game.active.setting.getEnvironment();
        if (environment) environment.death();
        
        var tavern = new ENV001TAV(this.game);
        tavern.play();
        
        super.play();
    }
    
}

export default SP001ESC;