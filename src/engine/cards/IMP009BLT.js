import { StandardImpediment } from './core/card.js';
import StandardBuff from './core/buff.js';
import StandardBounty from './core/bounty.js';
import StandardReplace from './core/replace.js';

const KILL_BUFF = "+1/+1";
const MULTIPLIER_BONUS = 10;
const ATTRIBUTES = { damage: 1, health: 1, bounty: 0 };
const DESCRIPTION = `
Kill: Enemy becomes a Black Tar with the same stats as this one.
Kill: ${KILL_BUFF} to all Black Tars
Death: +${MULTIPLIER_BONUS} * <Num all black tars> points to the active side.
Death: Kill all black tar.
Attack: Does not attack other black tar.
`;

const KillBuff = StandardBuff.fromString(KILL_BUFF);

class IMP009BLT extends StandardImpediment {
    constructor(game) { super(game, ATTRIBUTES); }

    get name() { return 'Black Tar' }
    get id() { return 'IMP009BLT' }
    get types() { return ['Monstrous', 'Black', 'Tar'].concat(super.types); }
    get description(){ return DESCRIPTION; }

    death(){
        StandardBounty.death(this, this.game);
        super.death();
    }

    attack(card){
        if (card.id == this.id) return;
        else super.attack(card);
    }

    kill(card) {
        this.replaceWithTar(card);
        this.buffAllTars();
        super.rest();
    }

    death(){
        super.death();
        var activeTar = this.allBlackTar().filter(c => c.active);
        if (activeTar.length > 0) {
            this.game.active.points.add(activeTar.length * MULTIPLIER_BONUS);
            activeTar.death();
        }
    }

    replaceWithTar(card){
        StandardReplace.replace(card, () => new IMP009BLT(this.game));
    }

    buffAllTars(){
        this.allBlackTar().forEach(KillBuff);
    }

    allBlackTar()
    {
        return this.game.active.heroes.filter(c => c && c.id == this.id).concat( 
            this.game.inactive.heroes.filter(c => c && c.id == this.id)).concat( 
            this.game.active.setting.filter(c => c && c.id == this.id)).concat( 
            this.game.inactive.setting.filter(c => c && c.id == this.id));
    }


}

export default IMP009BLT;