import { Impediment }  from '../card.js';
import StandardAttack from './core/attack.js';
import StandardDefend from './core/defend.js';
import StandardDeath from './core/death.js';
import StandardDiscard from './core/discard.js';
import ENV002PLN from './ENV002PLN.js';
import ENV003MRP from './ENV003MRP.js';

const ATTRIBUTES = { damage: 0, health: 10, bounty: 0 };
const DESCRIPTION = `
Play: Set the health of all active side non-mirror heroes to 1.
Play: Set the active side environment to the Mirror Plane.
Death: Clear the active board
Death: Set the active side environment to The Planes
`;

class IMP006TMR extends Impediment {

    constructor(game) {
        super(game); 
        this.attributes.set(ATTRIBUTES);
    }

    get name() { return 'The Mirror' }
    get id() { return 'IMP006TMR' }
    get types() { return ['Mirror', 'Terrifying'].concat(super.types); }
    get description() { return DESCRIPTION; }

    attack(card) {
        StandardAttack.execute(this, card);
        super.attack(card);
    }

    defend(card){
        StandardDefend.execute(this, card);
        super.defend(card);
    }

    play(){
        if (this.game.active.setting.getImpediment()) return;

        this.game.active.impediment = this;

        var mirrorPlane = new ENV003MRP(this.game);
        mirrorPlane.play();

        this.game.active.heroes.notOfType('Mirror').forEach((hero) => {
            if (!hero) return;
            hero.attributes.modify('health', 1);
        });

        super.play();
    }

    discard(){
        StandardDiscard.discard(this);
        super.discard();
    }

    death() {
        var active = this.game.active;

        this.game.active.heroes.forEach((hero) => {
            if (!hero) return;
            hero.death();
        });

        var plains = new ENV002PLN(this.game);
        plains.play();

        StandardDeath.death(this);
        super.death();
    }


}

export default IMP006TMR;