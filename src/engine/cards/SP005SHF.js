import { StandardSpell } from './core/card.js';
const DESCRIPTION = `
Play: All heroes on both sides shuffle forward one slot.
`;

class SP005SHF extends StandardSpell {

    get name(){ return 'Shuffle Forward'; }
    get id(){ return 'SP005SHF'; }
    get description(){ return DESCRIPTION; }

    play() {
        this.shuffleSide(this.game.active);
        this.shuffleSide(this.game.inactive)
        super.play();
    }

    shuffleSide(side) {
        var offense = side.offense;        
        side.offense = side.support;
        side.support = side.defense;
        side.defense = offense;
    }

}

export default SP005SHF;