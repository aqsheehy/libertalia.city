import { StandardSupportiveHero } from './core/card.js';
import StandardRaid from './core/raid.js';
import StandardBuff from './core/buff.js';

const RAID_DAMAGE_CONSTRAINT = 10;
const VICTORY_BUFF = "+1/+1";
const ATTRIBUTES = { damage: 3, health: 4, restless: false };
const DESCRIPTION = `
Victory: ${VICTORY_BUFF} to all active side Orks
Victory: Left most Ork with ${RAID_DAMAGE_CONSTRAINT}+ attack raids the inactive impediment
Defeat: Attacks left most hero, if it has more attack it retaliates
`;

const VictoryBuff = StandardBuff.fromString(VICTORY_BUFF);

class SUP001ORW extends StandardSupportiveHero {

    constructor(game) { super(game, ATTRIBUTES); }

    get name() { return 'Ork Warleader'; }
    get id() { return 'SUP001ORW'; }
    get types() { return ['Leader', 'Ork', 'Humanoid', 'Male'].concat(super.types); }
    get description() { return DESCRIPTION; }

    victory() {
        
        var orks = this.game.active.heroes.ofType('Ork');
        orks.forEach(VictoryBuff);

        var biggestOrks = sortByDamage(orks);
        var biggestOrk = biggestOrks[0];
        if (!biggestOrk) return;

        var attributes = biggestOrk.attributes;
        if (attributes.current.damage < RAID_DAMAGE_CONSTRAINT) return;
        
        StandardRaid.impediment(biggestOrk, this.game.inactive, (card, target) => {
            if (!target) this.game.inactive.impediment = biggestOrk;
            else target.attack(card)
        });

        super.victory();
    }

    defeat() {
        var side = this.game.active;
        var biggestHeroes = sortByDamage(side.heroes);

        if (biggestHeroes[0] == this)
            StandardRaid.raid(this, biggestHeroes[1]);

        else 
            StandardRaid.raid(this, biggestHeroes[0], (card, target) => {
                if (target) target.attack(card);
            });

        super.defeat();
    }
}

function sortByDamage(heroes){
    return heroes.sort((a, b) => {
        if (!a && !b) return 0;
        if (!a) return -1;
        if (!b) return 1;
        var aDamage = a.attributes.current.damage;
        var bDamage = b.attributes.current.damage;
        if (aDamage > bDamage) return -1;
        else if (bDamage > aDamage) return 1;
        else return 0;
    }).filter((hero) => { return hero != null });
}

export { ATTRIBUTES };
export default SUP001ORW;