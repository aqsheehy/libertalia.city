import { StandardSpell } from './core/card.js';
import {  CombativeDecorator } from '../card.js';
const DESCRIPTION = `
Play: The active side defensive heroes stealths until it rests.
`;

class SP003STB_Decorator extends CombativeDecorator {
    
    constructor(card){
        super(card);
        this.attributes.modify('isStealthed', true);

        card.game.events.subscribe('finished', () => {
            if (this.attributes.current.isStealthed) 
                this.attributes.modify('isStealthed', false);
        });
    }
    
    get active() {
        return !this.isStealthed() && super.active;
    }
    
    isStealthed() {
        return this.attributes.current.isStealthed;
    }
    
}

class SP003STB extends StandardSpell {
    
    get name() { return 'Step Back'; }
    get id() { return 'SP003STB'; }
    get description(){ return DESCRIPTION; }

    play() {
        if (this.game.active.defense) 
            this.game.active.defense = new SP003STB_Decorator(this.game.active.defense, this.game);
            
        super.play();
    }
    
}

export default SP003STB;