import { StandardSpell } from './core/card.js';

const DESCRIPTION = `
Play: Player 1 becomes Player 2 and visa-versa (OP).
`;

class SP002UPD extends StandardSpell {
    
    get name() { return 'Upside Down'; }
    get id() { return 'SP002UPD'; }
    get description(){ return DESCRIPTION; }

    play() {
        this.game.flipActiveSide();
        super.play();
    }
    
}

export default SP002UPD;