import { StandardDefensiveHero } from './core/card.js';

import StandardRestless from './core/restless.js';
import StandardRaid from './core/raid.js';
import StandardBuff from './core/buff.js';

const ATTRIBUTES = { damage: 4, health: 5, restless: false };
const PLAY_BUFF = "+1/+1";
const ATTACK_DEFEND_KILL_BUFF = "+1/0";
const DESCRIPTION = `
Play: This gains ${PLAY_BUFF} for all orks
Attack: This gains ${ATTACK_DEFEND_KILL_BUFF}
Defend: This gains ${ATTACK_DEFEND_KILL_BUFF}
Kill: All active side orks gain ${ATTACK_DEFEND_KILL_BUFF}
Restless: Raid the inactive side defense
`;

const MainBuff = StandardBuff.fromString(ATTACK_DEFEND_KILL_BUFF);

class DEF001BOR extends StandardDefensiveHero {
    
    constructor(game) { super(game, ATTRIBUTES); }
    
    get name() { return 'Big Orc' }
    get id() { return 'DEF001BOR' }
    get types() { return ['Positive', 'Warrior', 'Ork', 'Humanoid', 'Male'].concat(super.types); }
    get description() { return DESCRIPTION; }

    play(){
        var side = this.game.active;
        var orks = side.heroes.ofType('Ork');
        StandardBuff.buff(this, orks.length, orks.length);

        super.play();
    }

    attack(card) {
        MainBuff(this);
        super.attack(card);
    }

    defend(card){
        MainBuff(this);
        super.defend(card);
    }
    
    kill(card) {
        this.game.active.heroes.ofType('Ork').forEach(MainBuff);
    }
    
    victory(){
        StandardRestless.victory(this)
        super.victory();
    }
    
    idle(){
        StandardRestless.idle(this, () => {
            StandardRaid.defense(this, this.game.inactive, (card, target) => {
                if (!target) this.game.inactive.defense = this;
                else target.attack(card);
            });
        });

        super.idle();
    }
}

export { ATTRIBUTES }; 
export default DEF001BOR;