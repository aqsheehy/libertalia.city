import { StandardSupportiveHero } from './core/card.js';

const DEATH_BONUS = 100;
const DEATH_CONSTRAINT = 20;
const ATTRIBUTES = { damage: 1, health: 3, restless: false, totalPlays: 0 };
const DESCRIPTION = `
Death: ${DEATH_BONUS}+ points to active side if there have been ${DEATH_CONSTRAINT} plays since play.
Death: ${DEATH_BONUS} - (${DEATH_BONUS / DEATH_CONSTRAINT}xPlays) points subtracted from active side
`;

class SUP005MOL extends StandardSupportiveHero {

    constructor(game) { super(game, ATTRIBUTES); }

    get name() { return 'Martyr of Lore'; }
    get id() { return 'SUP005MOL'; }
    get types() { return ['Martyr', 'Priest', 'Lore'].concat(super.types); }
    get description() { return DESCRIPTION; }
    
    play() {
        super.play();
        
        this.game.events.subscribe('play', () => {
            if (!this.active) return;
            this.attributes.add('totalPlays', 1);
        });
    }

    death() {
        var totalPlays = this.attributes.current.totalPlays;

        if (totalPlays >= DEATH_CONSTRAINT)
            this.game.active.points.add(DEATH_BONUS);
        else 
            this.game.active.points.subtract(DEATH_BONUS - DEATH_BONUS / DEATH_CONSTRAINT * totalPlays);

        super.death();
    }
}

export default SUP005MOL;