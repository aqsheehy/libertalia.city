import { StandardSupportiveHero } from './core/card.js';

const DEATH_BONUS = 100;
const DEATH_CONSTRAINT = 5;
const ATTRIBUTES = { damage: 1, health: 3, restless: false, totalDiscards: 0 };
const DESCRIPTION = `
Death: ${DEATH_BONUS}+ points to active side if there have been ${DEATH_CONSTRAINT} discards since play.
Death: ${DEATH_BONUS} - (${DEATH_BONUS / DEATH_CONSTRAINT}xDiscards) points subtracted from active side
`;

class SUP004MOW extends StandardSupportiveHero {

    constructor(game) { super(game, ATTRIBUTES); }

    get name() { return 'Martyr of Waste'; }
    get id() { return 'SUP004MOW'; }
    get types() { return ['Martyr', 'Priest', 'Wasteful'].concat(super.types); }
    get description() { return DESCRIPTION; }

    play() {
        this.game.events.subscribe('discard', () => {
            if (!this.active) return;
            this.attributes.add('totalDiscards', 1);
        });
        super.play();
    }

    death() {
        var totalDiscards = this.attributes.current.totalDiscards;

        if (totalDiscards >= DEATH_CONSTRAINT)
            this.game.active.points.add(DEATH_BONUS);
        else 
            this.game.active.points.subtract(DEATH_BONUS - DEATH_BONUS / DEATH_CONSTRAINT * totalDiscards);

        super.death();
    }
}

export default SUP004MOW;