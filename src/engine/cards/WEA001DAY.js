import { StandardWeather } from './core/card.js';
import WEA002NIG from './WEA002NIG.js';
import StandardCombatLogic from './combat/standard.js';

class WEA001DAY extends StandardWeather {

    get id(){ return 'WEA001DAY'; }
    get name(){ return 'The Day'; }
    get types(){ return ['Bright'].concat(super.types); }

    rest() {
        var newWeather = new WEA002NIG(this.game);
        newWeather.play();
        super.rest();
    }

    getCombat() {
        return new StandardCombatLogic();
    }
}

export default WEA001DAY;