import { StandardImpediment } from './core/card.js';
import StandardBounty from './core/bounty.js';

const TAX_INCREMENT = 5;
const ATTRIBUTES = { damage: 0, health: 8, bounty: 0 };
const DESCRIPTION = `
Rest: Increase the tax amount by ${TAX_INCREMENT}
Rest: Steal the current tax amount points from both sides, add to bounty
Death: Add bounty to the active side points.
`;

class IMP005GKG extends StandardImpediment {

    constructor(game) { super(game, ATTRIBUTES); }

    get name() { return 'Greedy King' }
    get id() { return 'IMP005GKG' }
    get types() { return ['Greedy', 'Human', 'Male', 'Leader', 'King', 'Humanoid'].concat(super.types); }
    get description(){ return DESCRIPTION; }

    rest() {
        this.attributes.add('taxIncrement', TAX_INCREMENT);
        this.tax(this.game.active);
        this.tax(this.game.inactive);
        super.rest();
    }

    tax(side) {
        var taxIncrement = this.attributes.current.taxIncrement;
        var bounty = side.points.total < taxIncrement ? side.points.total : taxIncrement; 
        StandardBounty.add(this, bounty);
        side.points.subtract(taxIncrement);
    }

    death(){
        StandardBounty.death(this, this.game);
        super.death();
    }

}

export default IMP005GKG;