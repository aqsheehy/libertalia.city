import { StandardSpell } from './core/card.js';
import StandardRaid from './core/raid.js';

const DESCRIPTION= `
Play: If neither side has an impediment, play the left most impediment from the inactive hand.
Play: If the active side has an impediment it raids the inactive impediment or becomes it
`;

class SP006BAI extends StandardSpell {
    
    get name() { return 'Bait'; }
    get id() { return 'SP006BAI'; }
    get description(){ return DESCRIPTION; }

    play() {
        var activeImpediment = this.game.active.setting.getImpediment();
        var inactiveImpediment = this.game.inactive.setting.getImpediment();

        if (activeImpediment)
            StandardRaid.raid(activeImpediment, inactiveImpediment, (card, target) => {
                if (!target) this.game.inactive.impediment = activeImpediment;
            });

        if (!activeImpediment && !inactiveImpediment)
            this.game.drag(0, 'Impediment');

        super.play();
    }       
}

export default SP006BAI;