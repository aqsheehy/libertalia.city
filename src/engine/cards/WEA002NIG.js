import { StandardWeather } from './core/card.js';
import WEA001DAY from './WEA001DAY.js';
import RestingCombatLogic from './combat/rest.js';

class WEA002NIG extends StandardWeather {

    get id(){ return 'WEA002NIG'; }
    get name(){ return 'The Night'; }
    get types(){ return ['Dark'].concat(super.types); }
    
    rest() {
        var newWeather = new WEA001DAY(this.game);
        newWeather.play();
        super.rest();
    }

    getCombat() {
        return new RestingCombatLogic();
    }
}

export default WEA002NIG;