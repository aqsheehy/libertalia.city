import { StandardImpediment } from './core/card.js';
import IMP007MOR from './IMP007MOR.js';

const VICTORY_PENALTY = 100;
const ATTRIBUTES = { damage: 1, health: 2 };
const DESCRIPTION = `
Death: Play the left most hero from the active side hand. 
Death: Set the impediment to the Mother Roc.
Victory: -${VICTORY_PENALTY} points from the active side.
`;

class IMP001BAR extends StandardImpediment {
    
    constructor(game){ super(game, ATTRIBUTES); }

    get name() { return 'Baby Roc'; }
    get id(){ return 'IMP001BAR'; }
    get types() { return ['Monster', 'Fledgling'].concat(super.types) }
    get description(){ return DESCRIPTION; }

    death(){
        this.game.play(0, 'Hero');
        super.death();
        this.game.active.impediment = new IMP007MOR(this.game);
    }

    victory(){
        this.game.active.points.subtract(VICTORY_PENALTY);
        super.victory();
    }

}

export default IMP001BAR;