import { StandardImpediment } from './core/card.js';
import StandardBounty from './core/bounty.js';

const TAX_INCREMENT = 10;
const ATTRIBUTES = { damage: 0, health: 8, bounty: 0 };
const DESCRIPTION = `
Rest: Steal ${TAX_INCREMENT} points from both sides, add to bounty
Rest: If the either side couldn't pay: kill their left most hero
Death: Add bounty to the active side points.
`;

class IMP004MKG extends StandardImpediment {

    constructor(game) { super(game, ATTRIBUTES); }

    get name() { return 'Mad King' }
    get id() { return 'IMP004MKG' }
    get types() { return ['Mad', 'Human', 'Male', 'Leader', 'King', 'Humanoid'].concat(super.types); }
    get description(){ return DESCRIPTION; }

    rest() {
        this.tax(this.game.active);
        this.tax(this.game.inactive);
        super.rest();
    }

    tax(side) {

        if (side.points.total < TAX_INCREMENT) {
            StandardBounty.add(this, side.points.total);
            var leftMost = side.heroes.ofType('Hero')[0];
            if (leftMost) leftMost.death();
        }
        else
            StandardBounty.add(this, TAX_INCREMENT);

        side.points.subtract(TAX_INCREMENT);
        
    }

    death(){
        StandardBounty.death(this, this.game);
        super.death();
    }

}

export default IMP004MKG;