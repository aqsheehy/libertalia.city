import { StandardOffensiveHero } from './core/card.js';
import StandardJubilant from './core/jubilant.js';
import StandardRestless from './core/restless.js';
import WEA001DAY from './WEA001DAY.js';
import WEA003ORF from './WEA003ORF.js';

const ATTRIBUTES = { damage: 4, health: 1 };
const DESCRIPTION = `
Restless: Attack left most hero. If it dies: set active side weather to Ork Fog.
Jubilant: Set active side weather to Ork Fog.
Death: If the weather is Ork Fog, it becomes The Day
`;

class ATT002OPR extends StandardOffensiveHero {

    constructor(game) { super(game, ATTRIBUTES); }

    get name() { return 'Ork Primalist' }
    get id() { return 'ATT002OPR' }
    get types() { return ['Primalist', 'Ork', 'Humanoid', 'Male'].concat(super.types); }
    get description() { return DESCRIPTION; }

    idle() {
        StandardJubilant.idle(this);

        StandardRestless.idle(this, () => {

            var leftMost = this.game.active.heroes.ofType('Hero')[0];
            if (leftMost) this.attack(leftMost);

            if (!leftMost.active && this.active) 
            {
                this.game.active.weather = new WEA003ORF(this.game);
                this.game.inactive.weather = new WEA003ORF(this.game);
            }
        })
        
        super.idle();
    }

    victory() {

        StandardRestless.victory(this);

        StandardJubilant.victory(this, () => {
            this.game.active.weather = new WEA003ORF(this.game);
            this.game.inactive.weather = new WEA003ORF(this.game);
        });

        super.victory();
    }

    death() {

        if (this.game.active.setting.getWeather().id == 'WEA003ORF')
            this.game.active.weather = new WEA001DAY(this.game);

        if (this.game.inactive.setting.getWeather().id == 'WEA003ORF')
            this.game.inactive.weather = new WEA001DAY(this.game);
            
        super.death();
    }

}

export default ATT002OPR;