import { StandardOffensiveHero } from './core/card.js';
import StandardRestless from './core/restless.js';

const ATTRIBUTES = { damage: 6, health: 2, totalKills: 0 }
const DESCRIPTION = `
Kill: Play the first blood spell from the active hand
Restless: Attack self
Death: If this killed 3+ cards: play all blood spells cast, 
Death: If this killed < 3 cards: all active side heroes die.
`;

class ATT001MOB extends StandardOffensiveHero {

    constructor(game) { 
        super(game, ATTRIBUTES); 
        this.castSpells = [];
    }
    
    get name() { return 'Mad Ork Bloodletter' }
    get id() { return 'ATT001MOB' }
    get types() { return ['Blood', 'Mage', 'Ork', 'Humanoid', 'Male', 'Mad'].concat(super.types); }  
    get description() { return DESCRIPTION; }

    kill(card){
        this.attributes.add('totalKills', 1);
        var bloodSpell = this.game.active.hand.ofType('Blood', 'Spell')[0];
                
        if (bloodSpell) 
        {
            bloodSpell.play();
            this.castSpells.push(bloodSpell);
        }
    }

    victory(){
        StandardRestless.victory(this);
        super.victory();
    }

    idle() {
        StandardRestless.idle(this, () => {
            this.attack(this);
        });
        super.idle();
    }
    
    death() {

        if (this.attributes.current.totalKills >= 3)
            this.castSpells.forEach((spell) => spell.play());

        else
            this.game.active.heroes.forEach((hero) => {
                if (hero && hero != this) hero.death();
            });

        super.death();
    }
}

export default ATT001MOB;