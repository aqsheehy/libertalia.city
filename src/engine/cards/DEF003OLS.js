import { StandardDefensiveHero } from './core/card.js';
import StandardBounty from './core/bounty.js';

const PLAYER_TAX = 50;
const ATTRIBUTES = { damage: 1, health: 10 };
const DESCRIPTION = `
Idle: Attack the first card with Bounty. If there are none tax ${PLAYER_TAX} from active side.
`;

class DEF003OLS extends StandardDefensiveHero {

    constructor(game){ super(game, ATTRIBUTES); }

    get name() { return 'Ogre Loan Shark'; }
    get id() { return 'DEF003OLS'; }
    get types() { return ['Ogre', 'Bodyguard', 'Male', 'Humanoid'].concat(super.types); }
    get description() { return DESCRIPTION; }

    idle() {

        var bounty = 
            this.game.inactive.heroes.ofType('Hero').find(StandardBounty.get) ||
            this.game.inactive.setting.ofType('Impediment').find(StandardBounty.get) ||
            this.game.active.heroes.ofType('Hero').find(StandardBounty.get) ||
            this.game.active.setting.ofType('Impediment').find(StandardBounty.get);
        
        if (!bounty)
            this.game.active.points.subtract(PLAYER_TAX);
        else 
            this.attack(bounty);

        super.idle();
    }

}

export default DEF003OLS;