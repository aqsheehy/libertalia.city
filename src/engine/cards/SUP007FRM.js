import { StandardSupportiveHero } from './core/card.js';

const REST_BONUS = 50;
const ATTRIBUTES = { damage: 1, health: 4 };
const DESCRIPTION = `
Rest: ${REST_BONUS} points to both sides.
`;

class SUP007FRM extends StandardSupportiveHero {
    constructor(game) { super(game, ATTRIBUTES); }

    get name() { return 'Humble Farmer'; }
    get id() { return 'SUP006BNK'; }
    get types() { return [ 'Farmer', 'Male', 'Human', 'Humanoid' ].concat(super.types); }
    get description() { return DESCRIPTION; }

    rest(){
        this.game.active.points.add(REST_BONUS);
        this.game.inactive.points.add(REST_BONUS);
        super.rest();
    }
}

export default SUP007FRM;