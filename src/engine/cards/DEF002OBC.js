import { StandardDefensiveHero } from './core/card.js';

import StandardBuff from './core/buff.js';

const IDLE_PENALTY = 4;
const DEATH_BONUS = 2;
const ATTRIBUTES = { damage: 1, health: 5, totalDeaths: 0 };
const DESCRIPTION = `
Play: Gain 0/+1 * <Number of Deaths> in the game so far
Idle: -${IDLE_PENALTY} points from active side
On Death: +${DEATH_BONUS} points to the active side
`;

class DEF002OBC extends StandardDefensiveHero {

    constructor(game){ 
        super(game, ATTRIBUTES);

        game.events.subscribe('death', (evt) => {
            this.attributes.add('totalDeaths', 1);
            if (this.deployed)
                this.game.active.points.add(DEATH_BONUS);
        });
    }

    get name() { return 'Ork Bone Collector'; }
    get id() { return 'DEF002OBC'; }
    get types() { return ['Ork', 'Primalist'].concat(super.types); }
    get description() { return DESCRIPTION; }

    play(){
        this.deployed = true;
        StandardBuff.buff(this, 0, this.attributes.current.totalDeaths);
        super.play();
    }

    idle(){
        this.game.active.points.subtract(IDLE_PENALTY);
        super.idle();
    }
}

export { ATTRIBUTES };
export default DEF002OBC;