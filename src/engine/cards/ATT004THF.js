import { StandardOffensiveHero } from './core/card.js';
import StandardBounty from './core/bounty.js';

const STEAL_POINTS = 10;
const ATTRIBUTES = { damage: 2, health: 2, bounty: 0 }
const DESCRIPTION = `
Play: Spy - Plays into opposite board
Rest: Steals 10 points from the active player on rest.
Idle: Give cached points to inactive player
Defeat / Death: Give cached points to active player
`;

class ATT004THF extends StandardOffensiveHero {

    constructor(game) { super(game, ATTRIBUTES); }
    
    get name() { return 'Commoner Thief' }
    get id() { return 'ATT004THF' }
    get types() { return ['Spy', 'Human', 'Humanoid', 'Male'].concat(super.types); }  
    get description() { return DESCRIPTION; }

    rest(){
        var points = this.game.active.points;
        if (points.total > 0) {
            var tax = points.total < STEAL_POINTS ? points.total : STEAL_POINTS;
            points.subtract(tax);
            StandardBounty.add(this, tax);
        }
        super.rest();
    }

    idle(){
        this.game.inactive.points.add(StandardBounty.get(this));
        StandardBounty.set(this, 0);        
        super.idle();
    }

    death() {
        StandardBounty.death(this, this.game);
        super.death();
    }

    defeat(){
        StandardBounty.death(this, this.game);
        super.defeat();
    }
}

export default ATT004THF;