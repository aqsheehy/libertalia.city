import { StandardImpediment } from './core/card.js';
import StandardBuff from './core/buff.js';

const VICTORY_PENALTY = 50;
const DEATH_BONUS = 10;
const VICTORY_BUFF = "+1/+1"
const ATTRIBUTES = { damage: 3, health: 4 };
const DESCRIPTION = `
Play: Play the left most hero from the active side hand.
Idle: Play the left most hero from the active side hand.
Victory: Play the left most hero from the active side hand.
Victory: -${VICTORY_PENALTY} points from the active side.
Victory: Gain ${VICTORY_BUFF}
Death: +${DEATH_BONUS} points to the active side.
`;

const VictoryBuff = StandardBuff.fromString(VICTORY_BUFF);

class IMP003IMC extends StandardImpediment {
    
    constructor(game){ super(game, ATTRIBUTES); }

    get name() { return 'Impatient Challenger'; }
    get id(){ return 'IMP003IMC'; }
    get types(){ return ['Elf', 'Male', 'Humanoid'].concat(super.types); }
    get description(){ return DESCRIPTION; }

    play(){
        this.game.play(0, 'Hero');
        super.play();
    }

    death(){
        this.game.active.points.add(DEATH_BONUS);
        super.death();
    }

    victory() {
        VictoryBuff(this);

        this.game.active.points.subtract(VICTORY_PENALTY);
        this.game.play(0, 'Hero');
        super.victory();
    }

    idle() {
        this.game.play(0, 'Hero');
        super.idle();
    }
}

export default IMP003IMC;