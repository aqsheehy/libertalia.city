import { StandardEnvironment } from './core/card.js';
import StandardCombatLogic from './combat/standard.js';

class ENV001TAV extends StandardEnvironment {
    
    get name() { return 'The Tavern' }
    get id() { return 'ENV001TAV' }
    get types() { return ['Tavern', 'Positive'].concat(super.types); }
    get description(){ return 'Sets combat logic to standard' }
    
    getCombat(weather) { 
        return new StandardCombatLogic();
    }
}

export default ENV001TAV;