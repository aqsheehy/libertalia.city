import { Impediment }  from '../card.js';
import StandardAttack from './core/attack.js';
import StandardPlay from './core/play.js';
import StandardDeath from './core/death.js';
import StandardBuff from './core/buff.js';
import StandardDiscard from './core/discard.js';

const DEATH_PENALTY = 100;
const ATTRIBUTES = { damage: 0, health: 15 };
const DEFENSE_BUFF = "+1/0";
const ATTACK_BUFF = "0/+1";
const DESCRIPTION = `
Attack: ${ATTACK_BUFF} to the defender
Defend: ${DEFENSE_BUFF} to the attacker
Defend: If this has > 0 and <= 3 health: die with no penalty
Die: -${DEATH_PENALTY} points from the active side.
`;

const DefenseBuff = StandardBuff.fromString(DEFENSE_BUFF);
const AttackBuff = StandardBuff.fromString(ATTACK_BUFF);

class IMP002SPP extends Impediment {
    constructor(game){
        super(game);
        this.attributes.set(ATTRIBUTES);
    }

    get name() { return 'Sparring Partner'; }
    get id(){ return 'IMP002SPP'; }
    get types(){ return ['Human', 'Male', 'Humanoid'].concat(super.types); }
    get description(){ return DESCRIPTION; }

    play(){
        StandardPlay.execute(this, this.game);
        super.play();
    }

    attack(card){ 
        AttackBuff(card);
        StandardAttack.execute(this, card); 
        super.attack(card);
    }

    defend(card){ 
        DefenseBuff(card);
        var health = this.attributes.current.health;

        if (health <= 3 && health > 0) super.death();
        else if (health <= 0) this.death();

        super.defend();
    }

    discard(){
        StandardDiscard.discard(this);
        super.discard();
    }

    death() {
        this.game.active.points.subtract(DEATH_PENALTY);
        StandardDeath.death(this);
        super.death();
    }
}

export default IMP002SPP;