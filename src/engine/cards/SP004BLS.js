import { StandardSpell } from './core/card.js';

const DESCRIPTION = `
Play: For each side with a Blood Mage, kill the defensive slot of the opposite side.
`;

class SP004BLS extends StandardSpell {
    
    get name() { return 'Blood Sacrifice'; }
    get id() { return 'SP004BLS'; }
    get types() { return ['Blood'].concat(super.types); }
    get description() { return DESCRIPTION; }

    play() {
        this.tryExecute(this.game.active, this.game.inactive);
        this.tryExecute(this.game.inactive, this.game.active);
        super.play();
    }
    
    tryExecute(side, enemySide){
        
        var bloodMage = side.heroes.ofType('Blood', 'Mage')[0];
        if (!bloodMage) return;

        var enemy = enemySide.heroes.ofType('Hero')[0];
        if (enemy) enemy.death();

        if (bloodMage.active) bloodMage.death();
    }
    
}



export default SP004BLS;