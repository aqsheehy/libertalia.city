import { StandardEnvironment } from './core/card.js';
import MirrorCombatLogic from './combat/mirror.js';

const DESCRIPTION = `
Combat: 
- Each active side hero attacks the opposite slot.
- If the opposite slot lives, it attacks the active impediment.
`;

class ENV003MRP extends StandardEnvironment {
    
    get id(){ return 'ENV003MRP'; }
    get name(){ return 'Mirror Plane'; }
    get types(){ return ['Mirror', 'Desolate'].concat(super.types); }
    get description(){ return DESCRIPTION; }

    getCombat(weather) { return new MirrorCombatLogic(); }
}

export default ENV003MRP;