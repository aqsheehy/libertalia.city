import { StandardImpediment } from './core/card.js';

const VICTORY_PENALTY = 20;
const ATTRIBUTES = { damage: 4, health: 6 };
const DESCRIPTION = `
Victory: -${VICTORY_PENALTY} points from the active side.
`;

class IMP007MOR extends StandardImpediment {

    constructor(game) {super(game, ATTRIBUTES); }

    get name() { return 'Mother Roc'; }
    get id(){ return 'IMP007MOR'; }
    get types() { return ['Monster', 'Monstrous'].concat(super.types) }
    get description(){ return DESCRIPTION; }
    
    victory() {
        this.game.active.points.subtract(VICTORY_PENALTY);
        super.victory();
    }

}

export default IMP007MOR;