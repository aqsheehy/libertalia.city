import { OffensiveHero, DefensiveHero, SupportiveHero, Impediment, Spell, Weather, Environment } from '../../card.js';
import StandardAttack from './attack.js';
import StandardDefend from './defend.js';
import StandardPlay from './play.js';
import StandardDiscard from './discard.js';
import StandardDeath from './death.js';

class StandardDefensiveHero extends DefensiveHero {
    constructor(game, attributes) {
        super(game); 
        this.attributes.set(attributes);
    }

    play(){
        StandardPlay.execute(this, this.game);
        super.play();
    }

    discard() {
        StandardDiscard.discard(this);
        super.discard();
    }

    attack(card) {
        StandardAttack.execute(this, card, (c) => this.kill(c));
        super.attack(card);
    }

    kill(card){ }

    defend(card){
        StandardDefend.execute(this, card);
        super.defend(card);
    }

    death(){
        StandardDeath.death(this);
        super.death();
    }
}

class StandardOffensiveHero extends OffensiveHero {
    constructor(game, attributes) {
        super(game); 
        this.attributes.set(attributes);
    }

    play(){
        StandardPlay.execute(this, this.game);
        super.play();
    }

    discard() {
        StandardDiscard.discard(this);
        super.discard();
    }

    attack(card) {
        StandardAttack.execute(this, card, (c) => this.kill(c));
        super.attack(card);
    }

    kill(card){ }

    defend(card){
        StandardDefend.execute(this, card);
        super.defend(card);
    }

    death(){
        StandardDeath.death(this);
        super.death();
    }
}

class StandardSupportiveHero extends SupportiveHero {
    constructor(game, attributes) {
        super(game); 
        this.attributes.set(attributes);
    }

    play(){
        StandardPlay.execute(this, this.game);
        super.play();
    }

    discard() {
        StandardDiscard.discard(this);
        super.discard();
    }

    attack(card) {
        StandardAttack.execute(this, card, (c) => this.kill(c));
        super.attack(card);
    }

    kill(card){ }

    defend(card){
        StandardDefend.execute(this, card);
        super.defend(card);
    }

    death(){
        StandardDeath.death(this);
        super.death();
    }
}

class StandardImpediment extends Impediment {
    constructor(game, attributes) {
        super(game); 
        this.attributes.set(attributes);
    }

    play(){
        StandardPlay.execute(this, this.game);
        super.play();
    }

    discard() {
        StandardDiscard.discard(this);
        super.discard();
    }

    attack(card) {
        StandardAttack.execute(this, card, (c) => this.kill(c));
        super.attack(card);
    }

    kill(card){ }

    defend(card){
        StandardDefend.execute(this, card);
        super.defend(card);
    }

    death(){
        StandardDeath.death(this);
        super.death();
    }
}

class StandardSpell extends Spell {

    play(){
        StandardPlay.execute(this, this.game);
        super.play();
    }

    discard() {
        StandardDiscard.discard(this);
        super.discard();
    }

}

class StandardWeather extends Weather {
    play(){
        StandardPlay.execute(this, this.game);
        super.play();
    }

    discard() {
        StandardDiscard.discard(this);
        super.discard();
    }
}

class StandardEnvironment extends Environment {
    play(){
        StandardPlay.execute(this, this.game);
        super.play();
    }

    discard() {
        StandardDiscard.discard(this);
        super.discard();
    }
}

export { StandardDefensiveHero, StandardOffensiveHero, StandardSupportiveHero, StandardImpediment, StandardEnvironment, StandardWeather, StandardSpell };