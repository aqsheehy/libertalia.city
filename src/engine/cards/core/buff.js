class StandardBuff {

    static fromString(str) {
        var parts = str.split('/');
        var damage = parseInt(parts[0].replace(/\+/g, ""));
        var health = parseInt(parts[1].replace(/\+/g, ""));
        return function(card) { StandardBuff.buff(card, damage, health); };
    }

    static buff(card, damage, health) {
        var attr = card.attributes;
        if (damage !== 0) attr.add('damage', damage);
        if (health !== 0) attr.add('health', health);
    }

}

export default StandardBuff;