class StandardJubilant {
    static idle(card) {
        StandardJubilant.set(card, false)
    }
    
    static victory(card, onJubilant) {
        var attr = card.attributes;
        if (!StandardJubilant.get(card, attr)) 
            StandardJubilant.set(card, true, attr);
        else if (onJubilant)
            onJubilant();     
    }

    static get(card, attr){
        attr = attr || card.attributes;
        return attr.current.jubilant;
    }

    static set(card, value, attr){
        attr = attr || card.attributes;
        attr.modify('jubilant', value);
    }
}

export default StandardJubilant;