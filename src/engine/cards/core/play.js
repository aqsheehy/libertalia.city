class StandardPlay {
    static execute(card) {

        var game = card.game;
        var types = card.types;
        var isSpy = types.indexOf('Spy') > -1 ;

        var side = isSpy ? game.inactive : game.active;
        
        if (types.indexOf('Supportive') > -1)  {
            if (!side.support) side.support = card;
        }

        else if (types.indexOf('Offensive') > -1) {
            if (!side.offense) side.offense = card;
        }

        else if (types.indexOf('Defensive') > -1) {
            if (!side.defense) side.defense = card;
        }

        else if (types.indexOf('Impediment') > -1) {
            if (!side.impediment) side.impediment = card;
        }

        else if (types.indexOf('Environment') > -1)
            side.environment = card;

        else if (types.indexOf('Weather') > -1)
            side.weather = card;

        else if (types.indexOf('Spell') > -1)
            side.grave.push(card);

        else 
            throw new Error('Not played');
        
    }
}

export default StandardPlay;