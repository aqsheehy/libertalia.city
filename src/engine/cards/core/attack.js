class StandardAttack {
    static execute(source, destination, onKill){
        var sourceAttr = source.attributes;
        var destAttr = destination.attributes;
        if (!sourceAttr.current.damage || !destAttr.current.health) return;
        destAttr.subtract('health', sourceAttr.current.damage);
        
        destination.defend(source);
        if (onKill && !destination.active)
            onKill(destination);
    }
}

export default StandardAttack;