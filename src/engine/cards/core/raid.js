class StandardRaid {
    static raid(card, target, fn){
        if (!fn) fn = function(){};
        if (!target) fn(card)
        else { 
            card.attack(target); 
            if (target.active)
                fn(card, target);
            else
                fn(card);
        }
    }
    
    static offense(card, side, fn){
        var target = side.heroes.getOffense();
        StandardRaid.raid(card, target, fn);
    }

    static defense(card, side, fn){
        var target = side.heroes.getDefense();
        StandardRaid.raid(card, target, fn);
    }

    static support(card, side, fn){
        var target = side.heroes.getSupport();
        StandardRaid.raid(card, target, fn);
    }

    static impediment(card, side, fn) {
        var target = side.setting.getImpediment();
        StandardRaid.raid(card, target, fn);
    }
}

export default StandardRaid;