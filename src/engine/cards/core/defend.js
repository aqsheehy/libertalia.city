class StandardDefend {
    static execute(defending){
        var attributes = defending.attributes;
        if (attributes.current.health <= 0)
            defending.death();
    }
}

export default StandardDefend;