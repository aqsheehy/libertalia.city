class StandardDiscard {
    static discard(card) {
        card.location.home.grave.push(card);
    }
}

export default StandardDiscard;