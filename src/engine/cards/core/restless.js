class StandardRestless {
    static idle(card, onRestless) {
        var attr = card.attributes;
        
        if (!attr.current.restless) 
            attr.modify('restless', true);

        else if (onRestless)
            onRestless();
    }
    
    static victory(card) {
        var attr = card.attributes;
        attr.modify('restless', false);
    }
}

export default StandardRestless;