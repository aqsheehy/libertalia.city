import { CombativeDecorator } from '../../card.js';

class StandardBounty {

    static death(card, game){

        var attributes = card.attributes;
        var bounty = attributes.current.bounty;

        if (bounty === undefined) return;
        if (bounty === 0) return;

        game.active.points.add(bounty);
        attributes.current.bounty = 0;
    }

    static add(card, bounty){
        var attributes = card.attributes;
        attributes.add('bounty', bounty);
    }

    static get(card) {
        var attr = card.attributes;
        return attr.current.bounty;
    }

    static set(card, bounty){
        var attributes = card.attributes;
        attributes.modify('bounty', bounty);
    }

    static decorate(card, game){

        var attributes = card.attributes;
        var bounty = attributes.current.bounty;

        // If there is already a bounty, it can be assumed there is 
        // some process that will already evaluate it.
        return bounty === undefined 
            ? new CombativeBountyDecorator(card, game)
            : card;
    }
}

class CombativeBountyDecorator extends CombativeDecorator {

    death() {
        StandardBounty.death(this.source, this.game);
        super.death();
    }

}

export { CombativeBountyDecorator };
export default StandardBounty;