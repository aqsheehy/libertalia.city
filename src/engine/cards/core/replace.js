class StandardReplace {
    static replace(card, fn){
        var tombstone = card.location.history[card.location.history.length - 2];
        var side = card.game.sides[tombstone.player];

        if (tombstone.type == 'heroes') {
            if (tombstone.index == 0) side.defense = fn(side.defense);
            else if (tombstone.index == 1) side.offense = fn(side.offense);
            else if (tombstone.index == 2) side.support = fn(side.support);
        }
        else if (tombstone.type == 'setting'){
            if (tombstone.index == 0) side.environment = fn(side.environment);
            if (tombstone.index == 1) side.impediment = fn(side.impediment);
            if (tombstone.index == 2) side.weather = fn(side.weather);
        }
    }
}

export default StandardReplace;