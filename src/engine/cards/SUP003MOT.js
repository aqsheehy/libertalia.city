import { StandardSupportiveHero } from './core/card.js';

const DEATH_BONUS = 100;
const DEATH_CONSTRAINT = 10;
const ATTRIBUTES = { damage: 1, health: 3, restless: false, totalVictories: 0 };
const DESCRIPTION = `
Death: ${DEATH_BONUS}+ points to active side if there have been ${DEATH_CONSTRAINT} victories since play.
Death: ${DEATH_BONUS} - (${DEATH_BONUS / DEATH_CONSTRAINT}xDeaths) victories subtracted from active side
`;

class SUP003MOT extends StandardSupportiveHero {

    constructor(game) { super(game, ATTRIBUTES); }

    get name() { return 'Martyr of Triumph'; }
    get id() { return 'SUP003MOT'; }
    get types() { return ['Martyr', 'Priest', 'Triumphant'].concat(super.types); }
    get description() { return DESCRIPTION; }
    
    play() {
        this.game.events.subscribe('victory', () => {
            if (!this.active) return;
            this.attributes.add('totalVictories', 1);
        });
        super.play();
    }

    death() {
        var totalVictories = this.attributes.current.totalVictories;

        if (totalVictories >= DEATH_CONSTRAINT)
            this.game.active.points.add(DEATH_BONUS);
        else 
            this.game.active.points.subtract(DEATH_BONUS - DEATH_BONUS / DEATH_CONSTRAINT * totalVictories);

        super.death();
    }
}

export default SUP003MOT;