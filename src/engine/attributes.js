class AttributeStore {
    
    constructor(){
        this.root = { }
        this.current = { };
        this.modifications = [];
    }
    
    set(attributes) {
        this.root = attributes;
        this.current = Object.assign({}, attributes);
        this.modifications = [];
    }
    
    get() {
        return this.current;
    }
    
    /* 
        { op: 'modify', path: 'name', val: 'fweqf' }
        { op: 'add', path: 'name', val: 'fweqf' }
        { op: 'subtract', path: 'name', val: 'fweqf' }
        { op: 'delete', path: 'name' }
    */
    patch() {
        this.modifications.push.call(arguments);
        [].slice.call(arguments).forEach((change) => {
            switch (change.op) {
                case 'modify':
                    return this.current[change.path] = change.value;
                case 'delete':
                    return delete this.current[change.path];
                case 'add':
                    return this.current[change.path] = (this.current[change.path] || null) + change.value;
                case 'subtract':
                    return this.current[change.path] = (this.current[change.path] || null) - change.value;
                default:
                    throw Error(`Unsupported change type ${change.op}!`);
            }
        });
        return this;
    }

    modify(path, value){
        this.patch({ op: 'modify', path: path, value: value });
    }

    delete(path, value){
        this.patch({ op: 'delete', path: path, value: value });
    }

    add(path, value){
        this.patch({ op: 'add', path: path, value: value });
    }

    subtract(path, value){
        this.patch({ op: 'subtract', path: path, value: value });
    }
}

export default AttributeStore;