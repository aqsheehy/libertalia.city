// Functions required here as babel does not support extending Arrays...
function CardCollection (maxSize, player, type) {
    Array.apply(this, arguments);
    this.maxSize = maxSize; 
    this.player = player;
    this.type = type;
};

CardCollection.prototype = Object.create(Array.prototype);

CardCollection.prototype.removeAt = function(index) {
    this[index].location.clear();
    this.splice(index, 1);
    this.forEach((c, i) => c.location.setIndex(i));
}; 

CardCollection.prototype.remove = function(card){
    var index = this.indexOf(card);
    this.removeAt(index);
}

CardCollection.prototype.ofType = function() {
    var args = [].slice.call(arguments);
    return this.filter((card) => {
        return !!card && args.every((type) => card.types.indexOf(type) !== -1); 
    });
}; 

CardCollection.prototype.notOfType = function(type) {
    return this.filter((card) => {
        return !!card && card.types.indexOf(type) == -1; 
    });
}; 

CardCollection.prototype.modifyAll = function(fn){
    this.forEach((card, i) => {
        this[i] = fn(card);
    });
};

CardCollection.prototype.isFull = function(){
    return !!this.maxSize && this.length >= this.maxSize;
};

CardCollection.prototype.push = function(){
    
    if (!this.maxSize) return Array.prototype.push.apply(this, arguments); 
    
    var args = [].slice.call(arguments);
    if (this.length + args.length > this.maxSize) 
        throw Error('Max length exceeded');

    args.forEach(c => c.location.remove());
        
    var pushed = Array.prototype.push.apply(this, arguments);
    this.forEach((c, i) => {
        if (c) c.location.set(this.player, this.type, i);
    });
    return pushed;
};

CardCollection.prototype.concat = function(){
    return Array.prototype.concat.apply(this.slice(), arguments);
};

CardCollection.prototype.clearDead = function(){
    var died = [];
    var i = this.length;
    while (i--) {
        var card = this[i];
        if (card) {
            var attributes = this[i].attributes.get();
            if (attributes.hasDied) {
                died.push(card);
                this.splice(i, 1);
            }
        }
    }
    return died;
};

function HeroesCollection(player) {  
    CardCollection.call(this, 3, player, 'heroes');
    Array.prototype.push.call(this, null, null, null);
}

HeroesCollection.prototype = Object.create(CardCollection.prototype);

HeroesCollection.prototype.push = function(){ throw Error('Pushing is not supported'); }
HeroesCollection.prototype.pop = function(){ throw Error('Popping is not supported'); }
HeroesCollection.prototype.removeAt = function(index){ this[index] = null; }

const DEFENSE_SLOT = 0;
HeroesCollection.prototype.getDefense = function(){ return this[DEFENSE_SLOT]; };
HeroesCollection.prototype.setDefense = function(defense){ 
    if (defense) defense.location.remove();
    if (this[DEFENSE_SLOT]) this[DEFENSE_SLOT].location.clear();
    this[DEFENSE_SLOT] = defense; 
    if (defense) defense.location.set(this.player, this.type, DEFENSE_SLOT);
};

const OFFENSE_SLOT = 1;
HeroesCollection.prototype.getOffense = function(){ return this[OFFENSE_SLOT]; };
HeroesCollection.prototype.setOffense = function(offense){ 
    if(offense) offense.location.remove();
    if (this[OFFENSE_SLOT]) this[OFFENSE_SLOT].location.clear();
    this[OFFENSE_SLOT] = offense; 
    if (offense) offense.location.set(this.player, this.type, OFFENSE_SLOT);
};

const SUPPORT_SLOT = 2;
HeroesCollection.prototype.getSupport = function(){ return this[SUPPORT_SLOT]; };
HeroesCollection.prototype.setSupport = function(support){ 
    if (support) support.location.remove();
    if (this[SUPPORT_SLOT]) this[SUPPORT_SLOT].location.clear();
    this[SUPPORT_SLOT] = support; 
    if (support) support.location.set(this.player, this.type, SUPPORT_SLOT);
};

function SettingCollection(player){
    CardCollection.call(this, 3, player, 'setting');
    Array.prototype.push.call(this, null, null, null);
}

SettingCollection.prototype = Object.create(CardCollection.prototype);

SettingCollection.prototype.push = function(){ throw Error('Pushing is not supported'); }
SettingCollection.prototype.pop = function(){ throw Error('Popping is not supported'); }
SettingCollection.prototype.removeAt = function(index){ this[index] = null; }

const ENVIRONMENT_SLOT = 0;
SettingCollection.prototype.getEnvironment = function(){ return this[ENVIRONMENT_SLOT]; };
SettingCollection.prototype.setEnvironment = function(environment){ 
    if (environment) environment.location.remove();
    if (this[ENVIRONMENT_SLOT]) this[ENVIRONMENT_SLOT].location.clear();
    this[ENVIRONMENT_SLOT] = environment;
    if (environment) environment.location.set(this.player, this.type, ENVIRONMENT_SLOT);
}

const IMPEDIMENT_SLOT = 1;
SettingCollection.prototype.getImpediment = function(){ return this[IMPEDIMENT_SLOT]; };
SettingCollection.prototype.setImpediment = function(impediment){ 
    if (impediment) impediment.location.remove();
    if (this[IMPEDIMENT_SLOT]) this[IMPEDIMENT_SLOT].location.clear();
    this[IMPEDIMENT_SLOT] = impediment; 
    if (impediment)  impediment.location.set(this.player, this.type, IMPEDIMENT_SLOT);
};

const WEATHER_SLOT = 2;
SettingCollection.prototype.getWeather = function(){ return this[WEATHER_SLOT]; };
SettingCollection.prototype.setWeather = function(weather){ 
    if (weather) weather.location.remove();
    if (this[WEATHER_SLOT]) this[WEATHER_SLOT].location.clear();
    this[WEATHER_SLOT] = weather; 
    if (weather) weather.location.set(this.player, this.type, WEATHER_SLOT);
};

SettingCollection.prototype.clearDead = function(){
    var died = [];
    var i = this.length;
    while (i--) {
        var card = this[i];
        if (card) {
            var attributes = this[i].attributes.get();
            if (attributes.hasDied) {
                died.push(card);
                this[i] = null;
            }
        }
    }
    return died;
};

function Hand(player){
    CardCollection.call(this, 7, player, 'hand');
}

Hand.prototype = Object.create(CardCollection.prototype);

function Deck(player){
    CardCollection.call(this, 30, player, 'deck');
}

Deck.prototype = Object.create(CardCollection.prototype);

function Graveyard(player){
    CardCollection.call(this, 10000000000000, player, 'grave');
}

Graveyard.prototype = Object.create(CardCollection.prototype);

export { CardCollection, HeroesCollection, SettingCollection, Hand, Deck, Graveyard }