class Location {

    constructor(card) { 
        this.card = card; 
        this.history = [];
    }

    get player() { return this.$player; }
    get type() { return this.$type;  }
    get index() { return this.$index; }

    get home() { return this.card.game.sides[this.player]; }
    get opposite() { return this.card.game.sides[this.player == 'evens' ? 'odds': 'evens']; }

    replace(card) {  this.clear(); }

    set(player, type, index) {
        this.history.push({ player: player, type: type, index: index });
        this.$player = player;
        this.$type = type;
        this.$index = index;
    }

    setIndex(index){
        this.set(this.player, this.type, index);
    }

    clear() { this.set(null, null, null); }

    remove() {
        var side = this.card.game.sides[this.player];
        if (!side) return;
        var collection = side[this.type];
        if (!collection) return;
        collection.removeAt(this.$index);
    }
}

export default Location;