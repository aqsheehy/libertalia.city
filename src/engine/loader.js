import GameEngine from './game.js';
import CardCatalog from './catalog.js';

class GameLoader {
    static loadTestDeckAndRandomize(config){
        config.active.deck = shuffle(config.active.deck);
        config.inactive.deck = shuffle(config.inactive.deck);
        return GameLoader.loadTestDeck(config);
    }

    static loadTestDeck(config) {
        
        var game = new GameEngine();

        config.active.deck
            .map(CardCatalog.getCard)
            .map(type => new type(game))
            .forEach(game.active.deck.push);

        config.inactive.deck
            .map(CardCatalog.getCard)
            .map(type => new type(game))
            .forEach(game.inactive.deck.push);

        game.active.hand.push(game.active.deck[0]);
        game.active.hand.push(game.active.deck[0]);
        game.active.hand.push(game.active.deck[0]);
        game.active.hand.push(game.active.deck[0]);
        game.active.hand.push(game.active.deck[0]);

        game.inactive.hand.push(game.inactive.deck[0]);
        game.inactive.hand.push(game.inactive.deck[0]);
        game.inactive.hand.push(game.inactive.deck[0]);
        game.inactive.hand.push(game.inactive.deck[0]);
        game.inactive.hand.push(game.inactive.deck[0]);

        return game;
    }
}

function shuffle(cards){
    var currentIndex = cards.length, temporaryValue, randomIndex;

    while (0 !== currentIndex) {

        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        temporaryValue = cards[currentIndex];
        cards[currentIndex] = cards[randomIndex];
        cards[randomIndex] = temporaryValue;
    }

    return cards;
}

export default GameLoader