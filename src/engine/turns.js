import Turn from './turn.js';

class TurnManager {

    constructor(game, player1, player2){
        this.game = game;
        this.player1 = player1;
        this.player2 = player2;
        this.stopped = false;
        this.turns = [];
    }

    stop(){
        this.stopped = true;
    }

    async run() {
        var currentTurn = new Turn(this.game, this.player1);

        this.game.events.subscribe('play', (card) => console.log(`!!! [${card.name}] Played`));
        this.game.events.subscribe('deploy', (card) => console.log(`!!! [${card.name}] Deployed`));
        this.game.events.subscribe('idle', (card) => console.log(`!!! [${card.name}] Idled`));
        this.game.events.subscribe('rest', (card) => console.log(`!!! [${card.name}] Rested`));
        this.game.events.subscribe('draw', (card) => console.log(`!!! [${card.name}] Drawn`));
        this.game.events.subscribe('discard', (card) => console.log(`!!! [${card.name}] Discarded`));
        this.game.events.subscribe('death', (card) => console.log(`!!! [${card.name}] Died`));
        this.game.events.subscribe('attack', (card) => console.log(`!!! [${card.name}] Attacked`));
        this.game.events.subscribe('defend', (card) => console.log(`!!! [${card.name}] Defended`));
        this.game.events.subscribe('victory', (card) => console.log(`!!! [${card.name}] Victorious`));
        this.game.events.subscribe('defeat', (card) => console.log(`!!! [${card.name}] Defeated`));

        do 
        {
            console.log('');
            console.log(`# ---------------------------------------------------` );
            console.log(`# It is now your turn ${currentTurn.player.name} [Turn ${this.turns.length}] !!` );
            console.log(`# ---------------------------------------------------` );

            this.turns.push(currentTurn);

            await currentTurn.start();

            if (currentTurn.player == this.player1)
                currentTurn = new Turn(this.game, this.player2);
            else 
                currentTurn = new Turn(this.game, this.player1);
        }
        while (!this.stopped);
    }
}

export default TurnManager;