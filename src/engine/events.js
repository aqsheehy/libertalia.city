class EventBus {

    constructor(){
        this.listeners = {};
    }

    publish(type, data) {
        var listeners = this.listeners[type];
        if (!listeners) return;
        listeners.forEach((fn) => { fn(data); });
    }

    subscribe(type, fn) {
        var listeners = this.listeners[type];
        if (!listeners) this.listeners[type] = listeners = [];
        listeners.push(fn);
    }

}

export default EventBus;