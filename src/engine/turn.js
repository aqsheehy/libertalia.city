/*
    discard 1, draw 1, play 2
    discard 1, draw 2, play 0
    discard 0, draw 1, play 1
    discard 0, draw 2, play 0
*/
class Turn {
    constructor(game, player) {
        this.game = game;
        this.player = player;
        this.complete = false;
        this.decisions = [];
    }   

    async start() {

        var decision = await this.decideAndRun([ 'Discard', 'Draw', 'Finish' ]);           
            
        if (decision == 'Draw')
        {
            decision = await this.decideAndRun(['Draw', 'Play', 'Finish'])

            if (decision != 'Finish')
                decision = await this.decideAndRun(['Finish']);
        }
        else if (decision == 'Discard')
        {
            decision = await this.decideAndRun(['Draw']);

            if (decision != 'Finish')
            {
                decision = await this.decideAndRun(['Draw', 'Play', 'Finish']);
                
                if (decision == 'Play')
                    decision = await this.decideAndRun(['Play', 'Finish']);

                if (decision != 'Finish')
                    decision = await this.decideAndRun(['Finish']);
            }
        }
    }

    async decideAndRun(options) {
        var decision = await this.player.decide(options);
        this.processDecision(decision);
        return decision.type;
    }

    processDecision(decision){
        this.decisions.push(decision);

        switch (decision.type){
            case 'Discard':
                this.game.discard(decision.index);
                break;
            case 'Draw':
                this.game.draw();
                break;
            case 'Play':
                this.game.play(decision.index);
                break;
            case 'Finish':
                this.game.done();
                break;
        }   

        this.game.events.publish('decisionProcessed');
    }

}

export default Turn;