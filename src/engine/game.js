
import PlayerSide from './side.js';
import EventBus from './events.js';
import Points from './points.js';
import WEA002NIG from './cards/WEA002NIG.js';
import ENV002PLN from './cards/ENV002PLN.js';

class GameEngine {

    constructor() {
        this.sides = {
            evens: new PlayerSide('evens', this),
            odds: new PlayerSide('odds', this)
        };
        this.active = this.sides.evens;
        this.inactive = this.sides.odds;

        this.active.weather = new WEA002NIG(this);
        this.active.environment = new ENV002PLN(this);

        this.inactive.weather = new WEA002NIG(this);
        this.inactive.environment = new ENV002PLN(this);

        this.events = new EventBus();
        this.turn = 0;
    }

    flipActiveSide() {
        var active = this.active;
        this.active = this.inactive;
        this.inactive = active;
    }

    draw() {
        var player = this.active.key;
        var nextCard = this.active.deck[0];

        if (!this.active.hand.isFull())
            this.active.hand.push(nextCard);
        else
            this.active.grave.push(nextCard);

        nextCard.draw();
        return nextCard;
    }

    play(index, type) {
        var card = (type ? this.active.hand.ofType(type) : this.active.hand)[index];
        if (card) card.play();
        return card;
    }

    drag(index, type){
        // TODO Networking logic...
        var card = (type ? this.inactive.hand.ofType(type) : this.inactive.hand)[index];;
        if (card) card.play();
        return card;
    }

    discard(index) {
        var card = this.active.hand[index];
        if (!card) throw new Error(`No card at index ${index}`);
        card.discard();
        return card;
    }

    done() {
        var weather = this.active.weather;
        var environment = this.active.environment;
        var combat = environment.getCombat(weather);
        combat.execute(this.active, this.inactive);
        
        this.events.publish('finished');
        this.flipActiveSide();
        this.events.publish('start');
        this.turn++;
    }

    clearDead(){
        [].push.apply(this.active.grave, 
            this.active.heroes.clearDead()
                .concat(this.active.setting.clearDead()));

        [].push.apply(this.inactive.grave, 
            this.inactive.heroes.clearDead()
                .concat(this.inactive.setting.clearDead()));
    }
}

export default GameEngine;