import ATT001MOB from './cards/ATT001MOB.js';
import ATT002OPR from './cards/ATT002OPR.js';
import ATT003BNH from './cards/ATT003BNH.js';
import ATT004THF from './cards/ATT004THF.js';
import ATT005TRV from './cards/ATT005TRV.js';
import ATT006TRS from './cards/ATT006TRS.js';

import DEF001BOR from './cards/DEF001BOR.js';
import DEF002OBC from './cards/DEF002OBC.js';
import DEF003OLS from './cards/DEF003OLS.js';

import ENV001TAV from './cards/ENV001TAV.js';

import IMP001BAR from './cards/IMP001BAR.js';
import IMP002SPP from './cards/IMP002SPP.js';
import IMP003IMC from './cards/IMP003IMC.js';
import IMP004MKG from './cards/IMP004MKG.js';
import IMP005GKG from './cards/IMP005GKG.js';
import IMP006TMR from './cards/IMP006TMR.js';
import IMP007MOR from './cards/IMP007MOR.js';
import IMP008MQN from './cards/IMP008MQN.js';
import IMP009BLT from './cards/IMP009BLT.js';

import SP001ESC from './cards/SP001ESC.js';
import SP002UPD from './cards/SP002UPD.js';
import SP003STB from './cards/SP003STB.js';
import SP004BLS from './cards/SP004BLS.js';
import SP005SHF from './cards/SP005SHF.js';
import SP006BAI from './cards/SP006BAI.js';

import SUP001ORW from './cards/SUP001ORW.js';
import SUP002MOD from './cards/SUP002MOD.js';
import SUP003MOT from './cards/SUP003MOT.js';
import SUP004MOW from './cards/SUP004MOW.js';
import SUP005MOL from './cards/SUP005MOL.js';
import SUP006BNK from './cards/SUP006BNK.js';
import SUP007FRM from './cards/SUP007FRM.js';


class CardCatalog {
    static getCard(id) {
        switch (id) {
            case 'ATT001MOB': return ATT001MOB;
            case 'ATT002OPR': return ATT002OPR;
            case 'ATT003BNH': return ATT003BNH;
            case 'ATT004THF': return ATT004THF;
            case 'ATT005TRV': return ATT005TRV;
            case 'ATT006TRS': return ATT006TRS;

            case 'DEF001BOR': return DEF001BOR;
            case 'DEF002OBC': return DEF002OBC;
            case 'DEF003OLS': return DEF003OLS;

            case 'ENV001TAV': return ENV001TAV;

            case 'IMP001BAR': return IMP001BAR;
            case 'IMP002SPP': return IMP002SPP;
            case 'IMP003IMC': return IMP003IMC;
            case 'IMP004MKG': return IMP004MKG;
            case 'IMP005GKG': return IMP005GKG;
            case 'IMP006TMR': return IMP006TMR;
            case 'IMP007MOR': return IMP007MOR;
            case 'IMP008MQN': return IMP008MQN;

            case 'SP001ESC': return SP001ESC;
            case 'SP002UPD': return SP002UPD;
            case 'SP003STB': return SP003STB;
            case 'SP004BLS': return SP004BLS;
            case 'SP005SHF': return SP005SHF;
            case 'SP006BAI': return SP006BAI;

            case 'SUP001ORW': return SUP001ORW;
            case 'SUP002MOD': return SUP002MOD;
            case 'SUP003MOT': return SUP003MOT;
            case 'SUP004MOW': return SUP004MOW;
            case 'SUP005MOL': return SUP005MOL;
            case 'SUP006BNK': return SUP006BNK;
            
            default: return null;
        }
    }
}

export default CardCatalog;