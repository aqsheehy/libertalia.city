class GamePlayer {
    constructor(name, game, gameState) {
        this.$name = name;
        this.game = game;
        this.state = gameState;
    }

    get name() { return this.$name; }

    async decide(options)
    {
        var side = this.game.active;
        console.log('');
        this.printCollection('Heroes', side.heroes);
        this.printCollection('Setting', side.setting);
        this.printCollection('Hand', side.hand);
        console.log(`# Points (${side.points.total}),  Deck (${side.deck.length}), Grave (${side.grave.length})`);

        this.state.graveyard.events.onInputDown.removeAll();
        this.state.deck.events.onInputDown.removeAll();
        this.state.finish.events.onInputDown.removeAll();
        this.state.hand.forEach((c) => c.events.onInputDown.removeAll());
        
        console.log('# You can choose to either:', options);

        return new Promise((resolve, reject) => {

            if (options.length == 1 && options[0] == 'Draw')
                return resolve({ type: 'Draw' });

            if (options.indexOf('Finish') >= 0) {
                var finishListener = this.createListener({ type:'Finish' }, resolve);
                this.state.finish.events.onInputDown.add(finishListener, this);
            }

            if (options.indexOf('Discard') >= 0) {
                var graveListener = this.createListener({ type:'Discard', index: 0 }, resolve);
                this.state.graveyard.events.onInputDown.add(graveListener, this);
            }

            if (options.indexOf('Draw') >= 0) {
                var deckListener = this.createListener({ type:'Draw' }, resolve);
                this.state.deck.events.onInputDown.add(deckListener, this);
            }

            if (options.indexOf('Play') >= 0) {
                this.state.hand.forEach((card, i) => {
                    var cardListener = this.createListener({ type: 'Play', index: i }, resolve);
                    card.events.onInputDown.add(cardListener, this);
                });
            }

        });
    }

    printCollection(name, collection) {
        collection = collection.map(this.cardToString);
        var display = collection.length == 0 ? '<Empty>' : collection.length > 10 ? collection.slice(0, 10).join() : collection.join();
        console.log(`# ${name} (${collection.length}): ${display}`);
    }

    cardToString(c, i){
        if (!c) return `${i}:<Empty>`;
        var attr = c.attributes;
        var text = `${i}:${c.name}`;
        if (attr.current.health) {
            var damage = attr.current.damage === undefined ? '?' : attr.current.damage; 
            var health = attr.current.health === undefined ? '?' : attr.current.health;
            text += `[${damage}/${health}]`;
        }
        if (!c.active){
            text += "*D*"
        }
        return text;
    }
    
    createListener(config, resolve)
    {
        return function() { 
            console.log('Performed', config);
            resolve(config); 
        }
    }
}

export default GamePlayer;