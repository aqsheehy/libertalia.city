const SIDE_CARDS = 6;
const CARD_WIDTH = 150;
const CARD_PADDING = 5;
const PAIR_PADDING = 15;
const SIDE_WIDTH = PAIR_PADDING + (CARD_WIDTH + CARD_PADDING) * SIDE_CARDS;
const SIDE_HEIGHT = 400;
const SIDE_PADDING = 20;
const HAND_BOUND = (CARD_WIDTH + CARD_PADDING) * 7;

var CURRENT_WIDTH = 800;
var CURRENT_HEIGHT = 600;

class GameRenderer {

    constructor(game, gameState) {
        this.game = game;
        this.state = gameState;
        this.player1Side = new Phaser.Rectangle(0, 0, SIDE_WIDTH, SIDE_HEIGHT);
        this.player2Side = new Phaser.Rectangle(0, 0, SIDE_WIDTH, SIDE_HEIGHT);
        this.backpack = new Phaser.Rectangle(0, 0, CURRENT_WIDTH, SIDE_HEIGHT);
        this.backpackVisible = false;
    }

    init(width, height) {
        CURRENT_WIDTH = width;
        CURRENT_HEIGHT = height;
        this.state.init();
        this.game.stage.backgroundColor = "#fff";
        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.game.scale.setShowAll();
        this.backpack.y = CURRENT_HEIGHT - SIDE_HEIGHT - CARD_PADDING;
    }

    render() {
        this.game.debug.geom(this.player1Side,'rgba(0,0,0,0)');
        this.game.debug.geom(this.player2Side,'rgba(0,0,0,0)');
        this.game.debug.geom(this.backpack, 'rgba(0,0,0,0');
    }

    resize(width, height) {
        CURRENT_WIDTH = width;
        CURRENT_HEIGHT = height;
       
        this.game.scale.refresh(); 
        this.game.scale.setGameSize(CURRENT_WIDTH, CURRENT_HEIGHT);

        this.backpack.y = CURRENT_HEIGHT - SIDE_HEIGHT - CARD_PADDING;

        this.player1Side.x = (CURRENT_WIDTH - SIDE_WIDTH) / 2;
        this.player1Side.y = 20;

        this.state.player1.slots.forEach((c, i) => {
            c.position.x = this.player1Side.x + (CARD_PADDING + CARD_WIDTH) * i;
            if (i >= 3) c.position.x += PAIR_PADDING;
            c.position.y = this.player1Side.y;
        });

        this.player2Side.x = this.player1Side.x;
        this.player2Side.y = this.player1Side.y + SIDE_HEIGHT + SIDE_PADDING;

        this.state.player2.slots.forEach((c, i) => {
            c.position.x = this.player2Side.x + (CARD_PADDING + CARD_WIDTH) * i;
            if (i >= 3) c.position.x += PAIR_PADDING;
            c.position.y = this.player2Side.y;
        });        

        this.renderBackpack();
    }

    hideBackpack(){
        if (this.backpackVisible) {
            this.backpackVisible = false;
            this.renderBackpack();
        }
    }

    showBackpack() {
        if (!this.backpackVisible) {
            this.backpackVisible = true;
            this.renderBackpack();
        }
    }  

    renderBackpack() {
        var backpackY = this.backpackVisible ? this.backpack.y : -10000;

        this.state.graveyard.position.x = this.backpack.x + CARD_PADDING;
        this.state.graveyard.position.y = backpackY;

        this.state.deck.position.x = CURRENT_WIDTH - CARD_WIDTH - CARD_PADDING;
        this.state.deck.position.y = backpackY;

        var originX = (CURRENT_WIDTH - HAND_BOUND) / 2;
        this.state.hand.forEach((c, i) => {
            c.position.x = originX + (CARD_PADDING + CARD_WIDTH) * i;
            c.position.y = backpackY;
        });
    }  
}

export default GameRenderer;