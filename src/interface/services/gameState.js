const DEFAULT_SLOT_TYPE = 'CARD';
const DEFAULT_SLOT_X = -1000;
const DEFAULT_SLOT_Y = -1000;

const CARD_WIDTH = 150;
const SIDE_HEIGHT = 400;

class GameState {
    
    constructor(ui, game){
        this.ui = ui;
        this.game = game;
        this.player1 = { slots: new Array(6).fill(null) };
        this.player2 = { slots: new Array(6).fill(null) };
        this.hand = new Array(7).fill(null);
        this.graveyard = null;
        this.deck = null;
        this.finish = null;
    }

    init(){
        this.player1.slots = this.player1.slots.map(() => this.newSlot());
        this.player2.slots = this.player2.slots.map(() => this.newSlot());

        this.hand = this.hand.map(() => this.newSlot());
        this.graveyard = this.newSlot('Graveyard', 'Click to discard the left most card');
        this.deck = this.newSlot('Deck', 'Click to draw a card from the deck');
        this.finish = this.newButton(0, 0, 'FINISH');

        this.reflect();

        this.game.events.subscribe('decisionProcessed', () => this.reflect());
    }

    newButton(x, y, type){
        var button = this.ui.add.sprite(x, y, type);
        button.inputEnabled = true;
        return button;
    }

    newSlot(name, description){
        var card = this.ui.add.sprite(DEFAULT_SLOT_X, DEFAULT_SLOT_Y, DEFAULT_SLOT_TYPE);
        card.width = CARD_WIDTH;
        card.height = SIDE_HEIGHT;
        card.inputEnabled = true;

        var header = this.ui.add.text(10, 10, name || '<Empty>', { font: "30px Arial", fill: "black", align: 'left', wordWrap: true, wordWrapWidth: CARD_WIDTH});
        card.addChild(header);

        var description = this.ui.add.text(10, 120, description || '', {font: "20px Arial", fill: "black", align: 'left', wordWrap: true, wordWrapWidth: CARD_WIDTH});
        card.addChild(description);

        return card;
    }

    reflect() {
        var active = this.game.active;
        var inactive = this.game.inactive;

        this.hand.forEach((slot, i) => {
            var card = active.hand[i];
            slot.children[0].setText(card ? this.cardToString(card, i) : '<Empty>');
            slot.children[1].setText(card ? card.description || '' : '');
        });

        this.slots(active).forEach((card, i) => {
            var slot = this.player1.slots[i];
            slot.children[0].setText(card ? this.cardToString(card, i) : '<Empty>');
            slot.children[1].setText(card ? card.description || '' : '');
        });

        this.slots(inactive).forEach((card, i) => {
            var slot = this.player2.slots[i];
            slot.children[0].setText(card ? this.cardToString(card, i) : '<Empty>');
            slot.children[1].setText(card ? card.description || '' : '');
        });
    }

    slots(side){
        return [
            side.heroes.getSupport(),
            side.heroes.getOffense(),
            side.heroes.getDefense(),
            side.setting.getImpediment(),
            side.setting.getEnvironment(),
            side.setting.getWeather()
        ];
    }

    cardToString(c, i){
        if (!c) return `${i}:<Empty>`;
        var attr = c.attributes;
        var text = `${i}:${c.name}`;
        if (attr.current.health) {
            var damage = attr.current.damage === undefined ? '?' : attr.current.damage; 
            var health = attr.current.health === undefined ? '?' : attr.current.health;
            text += `\n[${damage}/${health}]`;
        }
        if (!c.active){
            text += "*D*"
        }
        return text;
    }
}

export default GameState;