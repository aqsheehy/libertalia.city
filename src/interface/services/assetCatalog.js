class AssetCatalog {
    static load(game) {
        game.load.image('CARD', '/assets/CARD.jpg');
        game.load.image('FINISH', '/assets/FINISH.jpg');
    }
}

export default AssetCatalog;
