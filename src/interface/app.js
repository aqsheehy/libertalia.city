import TurnManager from '../engine/turns.js';
import GameLoader from '../engine/loader.js';
import AssetCatalog from './services/assetCatalog.js';
import GameState from './services/gameState.js';
import GameRenderer from './services/gameRenderer.js';
import GamePlayer from './services/gamePlayer.js';

const SIDE_HEIGHT = 400;
var CURRENT_WIDTH = window.innerWidth * window.devicePixelRatio;
var CURRENT_HEIGHT = window.innerHeight * window.devicePixelRatio;

var game = new Phaser.Game(CURRENT_WIDTH, CURRENT_HEIGHT, Phaser.AUTO, '');
var gameEngine = GameLoader.loadTestDeckAndRandomize({
	"active": {
		"deck": [
			"ATT001MOB", "ATT002OPR", "ATT003BNH", "ATT004THF", "ATT005TRV", 
			"DEF001BOR", "DEF002OBC", "DEF003OLS", "ENV001TAV", "IMP001BAR", 
			"IMP002SPP", "IMP003IMC", "IMP004MKG", "IMP005GKG", "IMP006TMR",
			"IMP007MOR", "IMP008MQN", "SP002UPD", "SP001ESC", "SP002UPD",
			"SP003STB", "SP004BLS", "SP005SHF", "SP006BAI", "SUP001ORW",
			"SUP002MOD", "SUP003MOT", "SUP004MOW", "SUP005MOL", "SUP006BNK"
		]
	},
	"inactive": {
		"deck": [
			"ATT001MOB", "ATT002OPR", "ATT003BNH", "ATT004THF", "ATT005TRV", 
			"DEF001BOR", "DEF002OBC", "DEF003OLS", "ENV001TAV", "IMP001BAR", 
			"IMP002SPP", "IMP003IMC", "IMP004MKG", "IMP005GKG", "IMP006TMR",
			"IMP007MOR", "IMP008MQN", "SP002UPD", "SP001ESC", "SP002UPD",
			"SP003STB", "SP004BLS", "SP005SHF", "SP006BAI", "SUP001ORW",
			"SUP002MOD", "SUP003MOT", "SUP004MOW", "SUP005MOL", "SUP006BNK"
		]
	}
});
var gameState = new GameState(game, gameEngine);
var gameRenderer = new GameRenderer(game, gameState);

var player1 = new GamePlayer('Player 1', gameEngine, gameState);
var player2 = new GamePlayer('Player 2', gameEngine, gameState);
var turns = new TurnManager(gameEngine, player1, player2);

window.addEventListener('resize', () => { 
    CURRENT_WIDTH = window.innerWidth * window.devicePixelRatio;
    CURRENT_HEIGHT = window.innerHeight * window.devicePixelRatio; 
    gameRenderer.resize(CURRENT_WIDTH, CURRENT_HEIGHT); 
});

document.addEventListener("mousemove", (ev) => {
	var yPosition = ev.screenY * window.devicePixelRatio;
	if (yPosition > (CURRENT_HEIGHT - SIDE_HEIGHT)) {
		gameRenderer.showBackpack();
	}
	else {
		gameRenderer.hideBackpack();
	}
});

game.state.add('Board', { 
    preload: () => AssetCatalog.load(game), 

    create: () => { 
        gameRenderer.init();  
        gameRenderer.resize(CURRENT_WIDTH, CURRENT_HEIGHT);
        gameRenderer.showBackpack();
        turns.run();
    }, 

    render: () => gameRenderer.render() 
});

game.state.start('Board');